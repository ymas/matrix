﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json.Linq;

namespace Matrix.Data.BaseModels
{

    public abstract class BaseModel
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        [Column("Id",Order = 1)]
        public Guid Id { get; set; }
        
        [ScaffoldColumn(false)]
        [Column("DateTime",Order = 2)]
        [DataType(DataType.Text)]
        public DateTime? CreatedDate { get; set; }
        
        [ScaffoldColumn(false)]
        [Column("CreatedByName")]
        public string CreatedByName { get; set; }
        
        [ScaffoldColumn(false)]
        [Column("UserId")]
        public Guid? UserId { get; set; }
    }

    public abstract class Model : BaseModel
    {
        
    }

    public abstract class ModelDeletable : BaseModel
    {
        [ScaffoldColumn(false)]
        [Column("DeleteDate")]
        [DataType(DataType.Text)]
        public DateTime? DeleteDate { get; set; }
        
        [ScaffoldColumn(false)]
        [Column("DeleteByName")]
        public string DeleteByName { get; set; }
        
        [Column("IsDeleted")]
        [ScaffoldColumn(false)]
        public bool IsDeleted { get; set; }
    }

    public abstract class ModelModifiable : BaseModel
    {
        [ScaffoldColumn(false)]
        [Column("LastUpdateDate")]
        [DataType(DataType.Text)]
        public DateTime? LastUpdateDate { get; set; }
        
        [ScaffoldColumn(false)]
        [Column("LastUpdateBy")]
        public string LastUpdateBy { get; set; }
        [ScaffoldColumn(false)]
        public string UpdateHistory { get; set; }

        [ScaffoldColumn(false)]
        [Column("DeleteDate")]
        [DataType(DataType.Text)]
        public DateTime? DeleteDate { get; set; }
       
        [ScaffoldColumn(false)]
        [Column("DeleteByName")]
        public string DeleteByName { get; set; }
       
        [ScaffoldColumn(false)]
        [Column("IsDeleted")]
        public bool IsDeleted { get; set; }

        [ScaffoldColumn(false)]
        [Column("ActionDecription")]
        public string ActionDecription { get; set; }

        [NotMapped]
        public JArray JUpdatedHistory {
            get {
                try
                {
                    return JArray.Parse(UpdateHistory);
                }
                catch
                {
                    return JArray.Parse("[]");
                };
            }
        }
        
        public List<ModificationHistoryModelView> ModificationHistory()
        {
            var rtn = new List<ModificationHistoryModelView>();
            if (JUpdatedHistory != null)
            {
                foreach (JObject item in JUpdatedHistory)
                {
                    rtn.Add(new ModificationHistoryModelView()
                    {
                        Date = item.GetValue("date").ToString(),
                        By = item.GetValue("by").ToString(),
                        Action = item.GetValue("action").ToString()
                    }); 
                }
            }
            return rtn;
        }
    }



   public class ModificationHistoryModelView
    {
        public string Date { get; set; }
        public string By { get; set; }
        public string Action { get; set; }
    }
    
}