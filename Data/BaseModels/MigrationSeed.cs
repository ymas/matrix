﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Matrix.Data;
using Matrix.Data.DomainClasses;


namespace Matrix.Models
{
    
    public class MigrationSeed
    {
        private readonly ApplicationDbContext _context; 
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public MigrationSeed(ApplicationDbContext context , RoleManager<Role>  roleManager , UserManager<ApplicationUser>  userManager)
        {
            this._context = context;
            _userManager = userManager;
            _roleManager = roleManager;

        }
        
        public async Task Seeding()
        {

            // add Role admin
            if (!this._context.Roles.Any(a => a.Name == "Admin"))
            {
                await _roleManager.CreateAsync(new Role() {Id = Guid.NewGuid(), Description = "Admin Role", Name = "Admin"});
            }
            
            // add admin user
            if (!this._context.Users.Any(a => a.UserName == "Admin"))
            {
                // add default admin user with default password !QAZxsw2
                    var admin = await _userManager.CreateAsync(new ApplicationUser()
                    {
                        Id = Guid.NewGuid(),
                        Email = "Admin",
                        FullName = "System Admin",
                        RegisterDate = DateTime.Now,
                        IsDeleted = false,
                        LockoutEnabled = false,
                        UserName = "Admin"
                    }, "!QAZxsw2");

                    if (admin.Succeeded)
                    {
                        // add user admin to role admin 
                        var newAdmin = this._context.Users.FirstOrDefault(a => a.UserName == "Admin");
                        if (newAdmin != null)
                        {
                            // add admin use to admin role
                          await  _userManager.AddToRoleAsync(newAdmin, "Admin");
                        }
                    }
                
            }

        }
    }
}