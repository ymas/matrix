﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Newtonsoft.Json.Linq;

namespace Matrix.Data.DomainClasses
{
    
    public  class ApplicationUser : IdentityUser<Guid>
    {
       
      
       [DataType(DataType.Text)]
        public DateTime RegisterDate { get; set; }

        // auditing
        [DataType(DataType.Text)]
        public DateTime? DeleteDate { get; set; }
        public string DeleteByName { get; set; }
        public bool IsDeleted { get; set; }

        [DataType(DataType.Text)]
        public DateTime? LastUpdateDate { get; set; }
        public string UpdateByName { get; set; }
        private string UpdateHistory { get; set; }



        [Required(ErrorMessage = "Required")]
        [Display(Name = "FullName")]
        [Column("FullName")]
        public string FullName { get; set; }
        
        
        [Display(Name = "NameAr")]
        [Column("NameAr")]
        public string NameAr { get; set; }
        
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Gender")]
        [Column("Gender")]
        public string Gender { get; set; }
        
        [Display(Name = "Photo")]
        [Column("Photo")]
        public byte[] Photo { get; set; }

        [Display(Name = "EmployeeNo")]
        [Column("EmployeeNo")]
        public int? EmployeeNo { get; set; }

        public bool IsEnglishLanguage { get; set; }

        
      
        [NotMapped] public string FirstNameAr =>
            string.IsNullOrEmpty(NameAr) ? FullName.Split(' ').First() : NameAr.Split(' ').First();
        [NotMapped] public string FirstNameEn => string.IsNullOrEmpty(FullName) ? NameAr.Split(' ').First() : FullName.Split(' ').First();
        
        [NotMapped]
        public JArray JUpdatedHistory
        {
            get
            {
                try
                {
                    return JArray.Parse(UpdateHistory);
                }
                catch
                {
                    return JArray.Parse("[]");
                }
            }
        }


      
       
    }
    public class UserRole : IdentityUserRole<Guid>
    {
      
    }
    public class UserClaim : IdentityUserClaim<Guid>
    {
    }
    public class UserLogin : IdentityUserLogin<Guid>
    {
        public Guid Id { get; set; }
    }    
    public class UserToken : IdentityUserToken<Guid>
    {
    }    
    public class RoleClaim : IdentityRoleClaim<Guid>
    {
    }

    public class Role : IdentityRole<Guid>
    {
        public string Description { get; set; }

        public  enum RolesTypes
        {
            Admin,
            Auditor,
            KpiAdmin
        }
        

    }
    
    public class UserStore : UserStore<ApplicationUser,Role,ApplicationDbContext,Guid>
    {
        private readonly ApplicationDbContext _context;

        public UserStore(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }

    public class RoleStore : RoleStore<Role , ApplicationDbContext , Guid , UserRole , RoleClaim>
    {
        private readonly ApplicationDbContext _context;

        public RoleStore(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }
    }
}