

using System;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class StolenItemsDisposal : Model
    {
        public int StolenItemID { get; set; }
        public int StoenDisposalID { get; set; }
        public string DisposalDesc { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    
        public virtual StolenDisposal StolenDisposal { get; set; }
        public virtual StolenItem StolenItem { get; set; }
    }
}
