﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Matrix.Data.BaseModels;
using Matrix.Resources;

namespace Matrix.Data.DomainClasses
{
    [Table("Department")]
    public class Department : ModelDeletable
    {


        [Display(Name = "ParentDepartment" , ResourceType = typeof(_Translate))]
        [Column("ParentId")]
        public Guid? ParentId { get; set; }
        
        [Display(Name = "GrpId" , ResourceType = typeof(_Translate))]
        [Column("GrpId")]
        public int? GrpId { get; set; }
        
        [Required(ErrorMessageResourceName = "InsertDepartmentArabicName" , ErrorMessageResourceType = typeof(_Translate))]
        [StringLength(100)]
        [Display(Name = "DepartmentName" , ResourceType = typeof(_Translate))]
        [Column("NameAr")]
        public string NameAr { get; set; }
        
        [Required(ErrorMessageResourceName = "InsertDepartmentEnglishName", ErrorMessageResourceType = typeof(_Translate))]
        [StringLength(100)]
        [Display(Name = "DepartmentName" , ResourceType = typeof(_Translate))]
        [Column("NameEn")]
        public string NameEn { get; set; }
        
        [StringLength(100)]
        [Display(Name = "SupervisorName" , ResourceType = typeof(_Translate))]
        [Column("SupervisorName")]
        public string SupervisorName { get; set; }
        
                
        [StringLength(100)]
        [Display(Name = "SupervisorEmail" , ResourceType = typeof(_Translate))]
        [Column("SupervisorEmail")]
        public string SupervisorEmail { get; set; }

        // enum : dev , dep , unit,branch , sub_dep
        [Display(Name = "DepartmentType" , ResourceType = typeof(_Translate))]
        [Required(ErrorMessageResourceName = "SelectDepartmaentType", ErrorMessageResourceType = typeof(_Translate))]
        [StringLength(20)]
        [Column("Type")]
        public string Type { get; set; }
        
        
        public virtual ICollection<Department> ChildDepartments { get; set; }
        
        
        [ForeignKey("ParentId")]
        public virtual Department ParentDepartment { get; set; }
        
        [NotMapped]
        public string TrName => Utilties.TransableName(NameAr,NameEn);


        
      

    }
}    