﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Matrix.Data.BaseModels;
using Matrix.Resources;

namespace Matrix.Data.DomainClasses
{
    [Table("Organization")]
    public class Organization : ModelDeletable
    {
        [Required(ErrorMessageResourceName = "InsertOrganizationArabicName" , ErrorMessageResourceType = typeof(_Translate))]
        [StringLength(100)]
        [Display(Name = "OrganizationName" , ResourceType = typeof(_Translate))]
        [Column("NameAr")]
        public string NameAr { get; set; }
        
        [Required(ErrorMessageResourceName = "InsertOrganizationEnglishName", ErrorMessageResourceType = typeof(_Translate))]
        [StringLength(100)]
        [Display(Name = "OrganizationName" , ResourceType = typeof(_Translate))]
        [Column("NameEn")]
        public string NameEn { get; set; }

        [Display(Name = "Logo" , ResourceType = typeof(_Translate))]
        [Required(ErrorMessageResourceName = "InsertOrganizationLogo", ErrorMessageResourceType = typeof(_Translate))]
        [Column("Logo")]
        public byte[] Logo { get; set; }
        
                
        [NotMapped]
        public string TrName => Utilties.TransableName(NameAr,NameEn);
       
    }
}