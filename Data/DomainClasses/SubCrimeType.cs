

using System.Collections.Generic;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class SubCrimeType : ModelDeletable
    {
        
        public SubCrimeType()
        {
            Crimes = new HashSet<Crime>();
        }
    
        public int CrimeTypeID { get; set; }
        public string Name { get; set; }
    
        
        public virtual ICollection<Crime> Crimes { get; set; }
        public virtual CriminalType CriminalType { get; set; }
    }
}
