

using System;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class CrimeRecord : Model
    {
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime RecordDate { get; set; }
        public int RecordTypeID { get; set; }
        public string Description { get; set; }
        public byte[] Attachment { get; set; }
        public int CrimeID { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
    
        public virtual CrimeRecord_Type CrimeRecord_Type { get; set; }
        public virtual Crime Crime { get; set; }
    }
}
