using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class ProfilePhoto : ModelDeletable
    {
        public int ProfileId { get; set; }
        public byte[] Photo { get; set; }
    
        public virtual Profile Profile { get; set; }
    }
}
