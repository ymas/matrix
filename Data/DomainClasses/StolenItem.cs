

using System;
using System.Collections.Generic;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class StolenItem : ModelDeletable
    {
        
        public StolenItem()
        {
            StolenItemsDisposals = new HashSet<StolenItemsDisposal>();
        }
    
        public int CrimeId { get; set; }
        public int ItemCategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal ValuePerItem { get; set; }
        public int Quantity { get; set; }
        public bool? IsCaptuered { get; set; }
        public byte[] Image { get; set; }
        public DateTime? CapturingDate { get; set; }
    
        public virtual StealingCrime StealingCrime { get; set; }
        public virtual StolenItemCategory StolenItemCategory { get; set; }
        
        public virtual ICollection<StolenItemsDisposal> StolenItemsDisposals { get; set; }
    }
}
