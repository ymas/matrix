

using System.Collections.Generic;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class StolenDisposal : ModelDeletable
    {
        
        public StolenDisposal()
        {
            StolenItemsDisposals = new HashSet<StolenItemsDisposal>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<StolenItemsDisposal> StolenItemsDisposals { get; set; }
    }
}
