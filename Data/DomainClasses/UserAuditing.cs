using System;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class UserAuditing : Model
    {
        public int RefID { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string RefType { get; set; }
        public DateTime DateAndTime { get; set; }
        public string Action { get; set; }
        public string TraceRemarks { get; set; }
    }
}
