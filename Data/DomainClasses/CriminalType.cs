using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class CriminalType : ModelDeletable
    {
        
        public CriminalType()
        {
            Crimes = new HashSet<Crime>();
            SubCrimeTypes = new HashSet<SubCrimeType>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<Crime> Crimes { get; set; }
        
        public virtual ICollection<SubCrimeType> SubCrimeTypes { get; set; }
    }
}
