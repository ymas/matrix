using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Nationality : ModelDeletable
    {
        
        public Nationality()
        {
            Crime_Profile = new HashSet<Crime_Profile>();
            Profiles = new HashSet<Profile>();
            StealingCrimes = new HashSet<StealingCrime>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<Crime_Profile> Crime_Profile { get; set; }
        
        public virtual ICollection<Profile> Profiles { get; set; }
        
        public virtual ICollection<StealingCrime> StealingCrimes { get; set; }
    }
}
