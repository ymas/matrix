using System.Collections.Generic;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class StolenItemCategory : ModelDeletable
    {
        
        public StolenItemCategory()
        {
            StolenItems = new HashSet<StolenItem>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<StolenItem> StolenItems { get; set; }
    }
}
