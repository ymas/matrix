

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class dep : ModelDeletable
    {
        
        public dep()
        {
            Crimes = new HashSet<Crime>();
            StealingCrimes = new HashSet<StealingCrime>();
        }
    
        public string DepName { get; set; }
        public string DepCode { get; set; }
        public bool? IspoliceStation { get; set; }
        public string CriminalID { get; set; }
    
        
        public virtual ICollection<Crime> Crimes { get; set; }
        
        public virtual ICollection<StealingCrime> StealingCrimes { get; set; }
    }
}
