using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Crime_Impacts : Model
    {
        public int CrimeId { get; set; }
        public int ImpactId { get; set; }
    
        public virtual Crime Crime { get; set; }
        public virtual Impact Impact { get; set; }
    }
}
