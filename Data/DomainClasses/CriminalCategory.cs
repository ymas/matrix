using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class CriminalCategory : ModelDeletable
    {
        
        public CriminalCategory()
        {
            Profiles = new HashSet<Profile>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<Profile> Profiles { get; set; }
    }
}
