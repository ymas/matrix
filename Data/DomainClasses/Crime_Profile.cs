

using System;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Crime_Profile : Model
    {
        public int ProfileId { get; set; }
        public int CrimeId { get; set; }
        public string ProfileType { get; set; }
        public int NationalityId { get; set; }
        public int? ArrestedStatus { get; set; }
        public string ArrestedBy { get; set; }
        public DateTime? ArrestedDate { get; set; }
    
        public virtual Crime Crime { get; set; }
        public virtual Nationality Nationality { get; set; }
        public virtual Profile Profile { get; set; }
    }
}
