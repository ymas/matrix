using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Impact : ModelDeletable
    {
        
        public Impact()
        {
            Crime_Impacts = new HashSet<Crime_Impacts>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<Crime_Impacts> Crime_Impacts { get; set; }
    }
}
