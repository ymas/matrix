using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Countenance : ModelDeletable
    {
        
        public Countenance()
        {
            Profiles = new HashSet<Profile>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<Profile> Profiles { get; set; }
    }
}
