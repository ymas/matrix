

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Crime : ModelModifiable
    {
        
        public Crime()
        {
            Crime_Impacts = new HashSet<Crime_Impacts>();
            Crime_Profile = new HashSet<Crime_Profile>();
            Crime_Tools = new HashSet<Crime_Tools>();
            CrimeRecords = new HashSet<CrimeRecord>();
        }

        public int DepartmentId { get; set; }
        public DateTime? CrimeDate { get; set; }
        public TimeSpan? TimeFrom { get; set; }
        public TimeSpan? TimeTo { get; set; }
        public string CallNumber { get; set; }
        public int? CaseId { get; set; }
        public string Place { get; set; }
        public int CrimeMethodId { get; set; }
        public string MethodDescription { get; set; }
        public decimal? StolenValue { get; set; }
        public string Notes { get; set; }
        public string CriminalDescription { get; set; }
        public int? AreaId { get; set; }
        public bool IsGang { get; set; }
        public string GangName { get; set; }
        public int Number { get; set; }
        public int CrimeTypeId { get; set; }
        public bool IsKnown { get; set; }
        public int? PlaceStatusID { get; set; }
        public int? CriminalPlanID { get; set; }
        public string CriminalLook { get; set; }
        public int? PlaceTypeID { get; set; }
        public DateTime RecordDate { get; set; }
        public string CrimeStepsDescription { get; set; }
        public string CurrencyName { get; set; }
        public string CurrencyType { get; set; }
        public decimal? CurrencyTotal { get; set; }
        public bool? IsUnkown { get; set; }
        public int? SubCrimeTypeId { get; set; }
        public string CrimeMotive { get; set; }
        public string StatusFlag { get; set; }
        public string DepartmentCriminalID { get; set; }

        public virtual Area Area { get; set; }
        public virtual Case Case { get; set; }
        
        public virtual ICollection<Crime_Impacts> Crime_Impacts { get; set; }
        
        public virtual ICollection<Crime_Profile> Crime_Profile { get; set; }
        
        public virtual ICollection<Crime_Tools> Crime_Tools { get; set; }
        
        public virtual ICollection<CrimeRecord> CrimeRecords { get; set; }
        public virtual Criminal_Method Criminal_Method { get; set; }
        public virtual CriminalPlan CriminalPlan { get; set; }
        public virtual CriminalType CriminalType { get; set; }
        public virtual dep dep { get; set; }
        public virtual PlaceStatu PlaceStatu { get; set; }
        public virtual PlaceType PlaceType { get; set; }
        public virtual SubCrimeType SubCrimeType { get; set; }
        public string Types { get; set; }
        public string Category { get; set; }
        public string Details { get; set; }
        public string Description_Arabic { get; set; }
        public string valuess { get; set; }
        public string Quantity { get; set; }
        public string Role_Of_things { get; set; }
    }
}
