

using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class CriminalRiskLevel : ModelDeletable
    {
        public string Name { get; set; }
    }
}
