using System.Collections.Generic;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Area : ModelDeletable
    {
        public Area()
        {
            Crimes = new HashSet<Crime>();
        }
        public string Name { get; set; }
        public virtual ICollection<Crime> Crimes { get; set; }
    }
}
