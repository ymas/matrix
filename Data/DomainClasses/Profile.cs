

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Profile : ModelDeletable
    {
        
        public Profile()
        {
            Crime_Profile = new HashSet<Crime_Profile>();
            ProfilePhotos = new HashSet<ProfilePhoto>();
        }
    
        public string Name { get; set; }
        public int NationalityId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public long? FingerprintNumber { get; set; }
        public long? UNID { get; set; }
        public int? CriminalCategoryId { get; set; }
        public string Nickname { get; set; }
        public int CountenanceId { get; set; }
        public string Job { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public byte[] Photo { get; set; }
        public int? CityID { get; set; }
        public string StatusFlag { get; set; }
        public string Mobile { get; set; }
        public string SatusFlag { get; set; }
        public int? AreaID { get; set; }
        public string Remarks { get; set; }
        public string Makani { get; set; }

        public virtual Countenance Countenance { get; set; }
        
        public virtual ICollection<Crime_Profile> Crime_Profile { get; set; }
        public virtual CriminalCategory CriminalCategory { get; set; }
        public virtual Nationality Nationality { get; set; }
        
        public virtual ICollection<ProfilePhoto> ProfilePhotos { get; set; }
    }
}
