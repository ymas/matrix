

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class CrimeRecord_Type : ModelDeletable
    {
        
        public CrimeRecord_Type()
        {
            CrimeRecords = new HashSet<CrimeRecord>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<CrimeRecord> CrimeRecords { get; set; }
    }
}
