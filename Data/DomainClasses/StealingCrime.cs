

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class StealingCrime : ModelDeletable
    {
        
        public StealingCrime()
        {
            StolenItems = new HashSet<StolenItem>();
        }
    
        public int DepartmentId { get; set; }
        public int? CaseNumber { get; set; }
        public string FormNumber { get; set; }
        public string NoticeNumber { get; set; }
        public DateTime NoticeDateAndTime { get; set; }
        public string ComplainerName { get; set; }
        public int? ComplainerNationality { get; set; }
        public DateTime ReceptionDate { get; set; }
        public DateTime ReplyDate { get; set; }
        public string TransferDestination { get; set; }
        public string TransferNumber { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public int? CaseYear { get; set; }
    
        public virtual dep dep { get; set; }
        public virtual Nationality Nationality { get; set; }
        
        public virtual ICollection<StolenItem> StolenItems { get; set; }
    }
}
