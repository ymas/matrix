

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Criminal_Method : ModelDeletable
    {
        
        public Criminal_Method()
        {
            Crimes = new HashSet<Crime>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<Crime> Crimes { get; set; }
    }
}
