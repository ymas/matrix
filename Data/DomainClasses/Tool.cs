

using System.Collections.Generic;
using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Tool : ModelDeletable
    {
        
        public Tool()
        {
            Crime_Tools = new HashSet<Crime_Tools>();
        }
    
        public string Name { get; set; }
    
        
        public virtual ICollection<Crime_Tools> Crime_Tools { get; set; }
    }
}
