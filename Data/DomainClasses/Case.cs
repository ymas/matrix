using System.Collections.Generic;

namespace Matrix.Data.DomainClasses
{
    public class Case
    {
        
        public Case()
        {
            Crimes = new HashSet<Crime>();
        }
    
        public string Name { get; set; }
    
        public virtual ICollection<Crime> Crimes { get; set; }
    }
}
