using Matrix.Data.BaseModels;

namespace Matrix.Data.DomainClasses
{
    public class Crime_Tools: Model
    {
        public int CrimeId { get; set; }
        public int ToolId { get; set; }
    
        public virtual Crime Crime { get; set; }
        public virtual Tool Tool { get; set; }
    }
}
