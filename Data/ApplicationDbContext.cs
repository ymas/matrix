using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Matrix.Extensions;
using Matrix.Data.BaseModels;
using Matrix.Data.DomainClasses;
using Matrix.Data.SqlProceduersViews;
using Matrix.Models.ModelViews;
using Microsoft.AspNetCore.Identity;
using Ymas.Extensions;

namespace Matrix.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, Role, Guid>
    {
            private readonly IHttpContextAccessor _accessor;
    
            public ApplicationDbContext(DbContextOptions options, IHttpContextAccessor accessor)
                : base(options)
            {
                _accessor = accessor;
            }
    
            
    
            public override int SaveChanges()
            {
                var creatable = ChangeTracker.Entries<Model>().ToList();
                var modifiable = ChangeTracker.Entries<ModelModifiable>().ToList();
                var deletable = ChangeTracker.Entries<ModelDeletable>().ToList();
    
                // validate entity
                var entities = from e in ChangeTracker.Entries()
                    where e.State == EntityState.Added
                          || e.State == EntityState.Modified
                    select e.Entity;
                foreach (var entity in entities)
                {
                    Validator.ValidateObject(entity, new ValidationContext(entity));
                }
    
                if (!creatable.Any() && !modifiable.Any() && !deletable.Any())
                {
                    return base.SaveChanges();
                }
    
                var uName = _accessor.HttpContext.User.Identity.Name;
                var unId = _accessor.HttpContext.User.Identity.GetUserId().ToGuid();
                var now = DateTime.Now;
    
                foreach (var en in creatable)
                {
                    switch (en.State)
                    {
                        case EntityState.Added:
                            en.Entity.CreatedByName = uName;
                            en.Entity.CreatedDate = now;
                            en.Entity.UserId = unId;
                            break;
    
                        case EntityState.Modified:
                            en.Property(a => a.CreatedByName).IsModified = false;
                            en.Property(a => a.CreatedDate).IsModified = false;
                            break;
                        case EntityState.Detached:
                            break;
                        case EntityState.Unchanged:
                            break;
                        case EntityState.Deleted:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
    
                foreach (var en in modifiable)
                {
                    switch (en.State)
                    {
                        case EntityState.Added:
                            en.Entity.CreatedByName = uName;
                            en.Entity.CreatedDate = now;
                            en.Entity.UserId = unId;
                            break;
    
                        case EntityState.Modified:
                            en.Property(a => a.CreatedByName).IsModified = false;
                            en.Property(a => a.CreatedDate).IsModified = false;
                            en.Entity.LastUpdateBy = uName;
                            en.Entity.LastUpdateDate = now;
    
                            var updates = en.Entity.JUpdatedHistory;
                            if (updates.Count >= 100)
                            {
                                updates.RemoveAt(0);
                            }
    
                            updates.Add(JToken.FromObject(new
                            {
                                date = now,
                                by = uName,
                                action = string.IsNullOrEmpty(en.Entity.ActionDecription)
                                    ? "تعديل"
                                    : en.Entity.ActionDecription
                            }));
    
                            en.Entity.UpdateHistory = updates.ToString();
                            break;
    
                        case EntityState.Deleted:
                            en.Property(a => a.CreatedByName).IsModified = false;
                            en.Property(a => a.CreatedDate).IsModified = false;
                            en.Property(a => a.LastUpdateBy).IsModified = false;
                            en.Property(a => a.LastUpdateDate).IsModified = false;
                            en.Property(a => a.UpdateHistory).IsModified = false;
                            en.Entity.DeleteByName = uName;
                            en.Entity.DeleteDate = now;
                            en.Entity.IsDeleted = true;
                            en.State = EntityState.Modified;
                            break;
                    }
                }
    
                foreach (var en in deletable)
                {
                    switch (en.State)
                    {
                        case EntityState.Added:
                            en.Entity.CreatedByName = uName;
                            en.Entity.CreatedDate = now;
                            en.Entity.UserId = unId;
                            break;
    
                        case EntityState.Deleted:
                            en.Property(a => a.CreatedByName).IsModified = false;
                            en.Property(a => a.CreatedDate).IsModified = false;
                            en.Entity.DeleteByName = uName;
                            en.Entity.DeleteDate = now;
                            en.Entity.IsDeleted = true;
    
                            // a voide permently deletion and convert it to modifing operation
                            en.State = EntityState.Modified;
                            break;
                    }
                }
    
                return base.SaveChanges();
            }
    
            public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess,
                CancellationToken cancellationToken = new CancellationToken())
            {
                var creatable = ChangeTracker.Entries<Model>().ToList();
                var modifiable = ChangeTracker.Entries<ModelModifiable>().ToList();
                var deletable = ChangeTracker.Entries<ModelDeletable>().ToList();
    
                // validate entity
                var entities = from e in ChangeTracker.Entries()
                    where e.State == EntityState.Added
                          || e.State == EntityState.Modified
                    select e.Entity;
                foreach (var entity in entities)
                {
                    Validator.ValidateObject(entity, new ValidationContext(entity));
                }
    
                if (!creatable.Any() && !modifiable.Any() && !deletable.Any())
                {
                    return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
                }
    
                var uName = _accessor.HttpContext.User.Identity.Name;
                var unId = _accessor.HttpContext.User.Identity.GetUserId().ToGuid();
                var now = DateTime.Now;
    
                foreach (var en in creatable)
                {
                    switch (en.State)
                    {
                        case EntityState.Added:
                            en.Entity.CreatedByName = uName;
                            en.Entity.CreatedDate = now;
                            en.Entity.UserId = unId;
                            break;
    
                        case EntityState.Modified:
                            en.Property(a => a.CreatedByName).IsModified = false;
                            en.Property(a => a.CreatedDate).IsModified = false;
                            break;
                        case EntityState.Detached:
                            break;
                        case EntityState.Unchanged:
                            break;
                        case EntityState.Deleted:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
    
                foreach (var en in modifiable)
                {
                    switch (en.State)
                    {
                        case EntityState.Added:
                            en.Entity.CreatedByName = uName;
                            en.Entity.CreatedDate = now;
                            en.Entity.UserId = unId;
                            break;
    
                        case EntityState.Modified:
                            en.Property(a => a.CreatedByName).IsModified = false;
                            en.Property(a => a.CreatedDate).IsModified = false;
                            en.Entity.LastUpdateBy = uName;
                            en.Entity.LastUpdateDate = now;
    
                            var updates = en.Entity.JUpdatedHistory;
                            if (updates.Count >= 100)
                            {
                                updates.RemoveAt(0);
                            }
    
                            updates.Add(JToken.FromObject(new
                            {
                                date = now,
                                by = uName,
                                action = string.IsNullOrEmpty(en.Entity.ActionDecription)
                                    ? "تعديل"
                                    : en.Entity.ActionDecription
                            }));
    
                            en.Entity.UpdateHistory = updates.ToString();
                            break;
    
                        case EntityState.Deleted:
                            en.Property(a => a.CreatedByName).IsModified = false;
                            en.Property(a => a.CreatedDate).IsModified = false;
                            en.Property(a => a.LastUpdateBy).IsModified = false;
                            en.Property(a => a.LastUpdateDate).IsModified = false;
                            en.Property(a => a.UpdateHistory).IsModified = false;
                            en.Entity.DeleteByName = uName;
                            en.Entity.DeleteDate = now;
                            en.Entity.IsDeleted = true;
                            en.State = EntityState.Modified;
                            break;
                    }
                }
    
                foreach (var en in deletable)
                {
                    switch (en.State)
                    {
                        case EntityState.Added:
                            en.Entity.CreatedByName = uName;
                            en.Entity.CreatedDate = now;
                            en.Entity.UserId = unId;
                            break;
    
                        case EntityState.Deleted:
                            en.Property(a => a.CreatedByName).IsModified = false;
                            en.Property(a => a.CreatedDate).IsModified = false;
                            en.Entity.DeleteByName = uName;
                            en.Entity.DeleteDate = now;
                            en.Entity.IsDeleted = true;
    
                            // a voide permently deletion and convert it to modifing operation
                            en.State = EntityState.Modified;
                            break;
                    }
                }
    
                return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
            }
            
//            private void Log(ApplicationUser user , string service , string operation , string log , ApplicationDbContext db)
//            {
//                db.EventsLogs.Add(new EventsLog()
//                {
//                    Id = Guid.NewGuid(), UserName = string.IsNullOrEmpty(user.NameAr) ? user.NameAr : user.FullName,
//                    Log = log, Service = service, Operation = operation
//                });
//                db.SaveChanges();
//            }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Department
            modelBuilder.Entity<Department>()
                .ToTable("Department")
                .HasKey(a => a.Id);
            modelBuilder.Entity<Department>()
                .HasIndex(a => a.NameAr)
                .IsUnique();
            modelBuilder.Entity<Department>()
                .HasIndex(a => a.NameEn)
                .IsUnique();
            modelBuilder.Entity<Department>()
                .HasOne(a => a.ParentDepartment)
                .WithMany(a => a.ChildDepartments);
            modelBuilder.Entity<Department>()
                .HasQueryFilter(a => !a.IsDeleted);
            
          
            //UsersRoles
            modelBuilder.Entity<IdentityUserRole<Guid>>()
                .HasKey(r => new {r.UserId, r.RoleId});

            //UserLogin
            modelBuilder.Entity<IdentityUserLogin<Guid>>()
                .HasKey(l => new {l.LoginProvider, l.ProviderKey, l.UserId});

            //UserTokens
            modelBuilder.Entity<IdentityUserToken<Guid>>()
                .HasKey(l => new {l.LoginProvider, l.UserId});
        }
        public DbSet<Department> Departments { get; set; }
        
        
        // public virtual DbSet<Area> Areas { get; set; }
        // public virtual DbSet<Case> Cases { get; set; }
        // public virtual DbSet<city> cities { get; set; }
        // public virtual DbSet<Countenance> Countenances { get; set; }
        // public virtual DbSet<Crime_Impacts> Crime_Impacts { get; set; }
        // public virtual DbSet<Crime_Profile> Crime_Profile { get; set; }
        // public virtual DbSet<Crime_Tools> Crime_Tools { get; set; }
        // public virtual DbSet<CrimeRecord> CrimeRecords { get; set; }
        // public virtual DbSet<CrimeRecord_Type> CrimeRecord_Type { get; set; }
        // public virtual DbSet<Crime> Crimes { get; set; }
        // public virtual DbSet<Criminal_Method> Criminal_Method { get; set; }
        // public virtual DbSet<CriminalCategory> CriminalCategories { get; set; }
        // public virtual DbSet<CriminalPlan> CriminalPlans { get; set; }
        // public virtual DbSet<CriminalRiskLevel> CriminalRiskLevels { get; set; }
        // public virtual DbSet<CriminalType> CriminalTypes { get; set; }
        // public virtual DbSet<dep> deps { get; set; }
        // public virtual DbSet<Effects_Remains> Effects_Remains { get; set; }
        // public virtual DbSet<Impact> Impacts { get; set; }
        // public virtual DbSet<Nationality> Nationalities { get; set; }
        // public virtual DbSet<PlaceStatu> PlaceStatus { get; set; }
        // public virtual DbSet<PlaceType> PlaceTypes { get; set; }
        // public virtual DbSet<ProfilePhoto> ProfilePhotos { get; set; }
        // public virtual DbSet<Profile> Profiles { get; set; }
        // public virtual DbSet<StealingCrime> StealingCrimes { get; set; }
        // public virtual DbSet<StolenDisposal> StolenDisposals { get; set; }
        // public virtual DbSet<StolenItemCategory> StolenItemCategories { get; set; }
        // public virtual DbSet<StolenItem> StolenItems { get; set; }
        // public virtual DbSet<StolenItemsDisposal> StolenItemsDisposals { get; set; }
        // public virtual DbSet<SubCrimeType> SubCrimeTypes { get; set; }
        // public virtual DbSet<UserAuditing> UserAuditings { get; set; }
        // public virtual DbSet<Tool> Tools { get; set; }
    }
}