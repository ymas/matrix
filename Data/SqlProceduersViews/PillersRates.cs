using System;

namespace Matrix.Data.SqlProceduersViews
{
    public class PillersRates
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double? KpisRate { get; set; }
        public double? EnablersRate { get; set; }
        public double? OverAllRate { get; set; }
        public double TasksRate { get; set; }
    }
}