using System;

namespace Matrix.Data.SqlProceduersViews
{
    public class KpisProcView
    {
        public Guid Id { get; set; }
        public string NameAr { get; set; }
    }


    public class OverAllRateProc
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public double Rate { get; set; }
    }
}