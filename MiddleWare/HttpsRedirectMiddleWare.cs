using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Matrix.MiddleWare
{
    
    public static class HttpsRedirectMiddlewareAppBuilderExtension
    {
        public static IApplicationBuilder HttpsRedirectMiddleWare(this IApplicationBuilder app)
        {
            return app.UseMiddleware<HttpsRedirectMiddleWare>();
        }
    }
    public  class HttpsRedirectMiddleWare
    {
        private readonly RequestDelegate _requestDelegate;

        public HttpsRedirectMiddleWare(RequestDelegate requestDelegate)
        {
            _requestDelegate = requestDelegate;
        }

        public async  Task Invoke(HttpContext context)
        {
            var sHost = context.Request.Host.HasValue ? context.Request.Host.Value : "";  //domain without :80 port .ToString();
            sHost = sHost.ToLower();
            var sPath = context.Request.Path.HasValue? context.Request.Path.Value:"";
            var sQuerystring = context.Request.QueryString.HasValue ? context.Request.QueryString.Value : "";
            //----< check https >----
            // check if the request is *not* using the HTTPS scheme
            if (!context.Request.IsHttps)
            {
                //--< is http >--
                var newHttpsUrl = "https://" + sHost ;

                if (sPath != "")

                {

                    newHttpsUrl = newHttpsUrl + sPath;

                }

                if (sQuerystring != "")

                {

                    newHttpsUrl = newHttpsUrl +  sQuerystring;

                }
                context.Response.Redirect(newHttpsUrl);
            }

            if (sHost.IndexOf("www.", StringComparison.Ordinal)==0)
            {
                var newUrlWithoutWww = "https://" + sHost.Replace("www.","") ;
                if (sPath != "")
                {
                    newUrlWithoutWww = newUrlWithoutWww + sPath;
                }
                if (sQuerystring != "")
                {
                    newUrlWithoutWww = newUrlWithoutWww + sQuerystring;
                }
                context.Response.Redirect(newUrlWithoutWww);
            }
            
            await _requestDelegate.Invoke(context);
        }

       

    }
}