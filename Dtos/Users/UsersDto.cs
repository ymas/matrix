using System;
using System.Linq.Expressions;

namespace Matrix.Dtos.Users
{
    public class UsersDto
    {
        public static Expression<Func<Data.DomainClasses.ApplicationUser, UsersDto>> Mapper(bool isEn=false)  
        {
            return a => new UsersDto()
            {
                Id = a.Id,
                UserName = a.UserName,
                Name = isEn ? a.FullName : a.NameAr ?? a.FullName,
                StaffNumber = a.EmployeeNo,
                Email = a.Email,
                RegisterDate = a.RegisterDate,
                Gender = a.Gender,
                Photo = a.Photo
            };

        }
            
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int? StaffNumber { get; set; }
        public DateTime RegisterDate { get; set; }
        public string Roles { get; set; }
        public string Gender { get; set; }
        public byte[] Photo { get; set; }
    }
}