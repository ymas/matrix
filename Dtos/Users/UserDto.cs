using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore.Internal;

namespace Matrix.Dtos.Users
{
    public class UserDto
    {
        public static Expression<Func<Data.DomainClasses.ApplicationUser, UserDto>> Mapper(bool isEn=false)  
        {
            return a => new UserDto()
            {
                Id = a.Id,
                Name = isEn ? a.FullName : a.NameAr ?? a.FullName,
                StaffNumber = a.EmployeeNo,
                Email = a.Email,
            };

        }
            
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int? StaffNumber { get; set; }

    }
}