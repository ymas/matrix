using System;
using System.Linq.Expressions;

namespace Matrix.Dtos.Departments
{
    public class DepartmentsDto
    {
        public static Expression<Func<Data.DomainClasses.Department, DepartmentsDto>> Mapper(bool isEn=false)  
        {
            return a => new DepartmentsDto()
            {
                Id = a.Id,
                Name = isEn ? a.NameEn : a.NameAr
            };

        }
            
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}