﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Matrix.General
{
    public class Email
    {
        private readonly SmtpClient _client;
        public Email()
        {
            _client = new SmtpClient
            {
                Host = "netmail.coas.gov.ae",
                Port = 587,
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential("noreply@dcas.gov.ae", "!qaz@Dcas_2019")
            };
        }

        

        public Task<bool> SendEmail(string subject, string body, params string[] to)
        {
            var mail = new MailMessage
            {
                From = new MailAddress("4g@dcas.gov.ae", "4G Model"),
                IsBodyHtml = true,
                Subject = subject,
                Body = body
            };

            return Task.Run(() =>
            {
                try
                {
                    foreach (var s in to)
                    {
                        mail.To.Add(s);
                        _client.Send(mail);
                        mail.To.Clear();
                    }
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            });
        }

    }
}