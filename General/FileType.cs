﻿namespace Matrix.Models
{
    public class FileType
    {
        public string Extention { get; set; }
        public string MimeType { get; set; }
        public string IconePath { get; set; }
    }



}