﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Matrix
{
    public  class StringModal
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
    public static class TeamMemberRoles
    {
        public static string Leader = "Leader";
        public static string Member = "Member";
        private static List<StringModal> ToList = new List<StringModal>() {
            new StringModal(){ Value="leader" , Text=Constants.FindResByName(Leader) },
            new StringModal(){ Value="member" , Text=Constants.FindResByName(Member) },
        };

        public static SelectList _SelectList(string selected)
        {
            return new SelectList(ToList.Select(a => new SelectListItem() { Text=a.Text , Value=a.Value }), "Value", "Text" , selected);
        }

        public static string FindById(string id)
        {
            var res= ToList.FirstOrDefault(a => a.Value == id);
            if (res == null) return "UnKnown";
            return res.Text;
        }
    }
}