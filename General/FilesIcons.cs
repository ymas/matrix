﻿using System.Collections.Generic;
using System.Linq;
using Matrix.Helpers;
using Matrix.Models;

namespace Matrix.General
{
    public static class FilesIcons
    {
        public static List<FileType> Icons
        {
            
            get
            {
                return new List<FileType>()
            {
                new FileType(){ 

                    // word
                    MimeType="application/vnd.openxmlformats-officedocument.wordprocessingml.document" , 
                    IconePath="/images/icons/files/word.png",
                    Extention = ".docx"
                },
                    
                
                    new FileType() { Extention = ".jpg", MimeType = "image/jpeg", IconePath = "/images/icons/files/image.png"},
                    new FileType() { Extention = ".gif", MimeType = "image/dif", IconePath = "/images/icons/files/image.png" },
                    new FileType() { Extention = ".png", MimeType = "image/png", IconePath = "/images/icons/files/image.png" },

                
                new FileType(){ 
                    MimeType="application/msword" ,
                    Extention = ".doc",
                    IconePath="/images/icons/files/word.png"},
                new FileType(){ 
                    MimeType="application/vnd.ms-word.document.macroEnabled.12" ,
                    Extention = ".doc",
                    IconePath="/images/icons/files/word.png"},
                new FileType(){ 
                    MimeType="application/vnd.openxmlformats-officedocument.wordprocessingml.document" ,
                    Extention = ".docs",
                    IconePath="/images/icons/files/word.png"},
                
                
                    // power point
                    new FileType(){ 
                    MimeType="application/vnd.openxmlformats-officedocument.presentationml.presentation" ,
                     Extention = ".pptx",
                    IconePath="/images/icons/files/powerpoint.png"},
                    new FileType(){ 
                    MimeType="application/mspowerpoint" , 
                    Extention = ".pptx",
                    IconePath="/images/icons/files/powerpoint.png"},
                    new FileType(){ 
                    Extention = ".pptx",
                    MimeType="application/vnd.ms-powerpoint" , 
                    IconePath="/images/icons/files/powerpoint.png"},
                    new FileType(){
                        Extention = ".pptx",
                    MimeType="application/powerpoint" , 
                    IconePath="/images/icons/files/powerpoint.png"},
                    new FileType(){ 
                        Extention = ".pptx",
                    MimeType="application/x-mspowerpoint" , 
                    IconePath="/images/icons/files/powerpoint.png"},
                
                new FileType(){ 
                    MimeType="application/vnd.openxmlformats-officedocument.presentationml.presentation" ,
                    Extention = ".ppt",
                    IconePath="/images/icons/files/powerpoint.png"},
                new FileType(){ 
                    MimeType="application/mspowerpoint" , 
                    Extention = ".ppt",
                    IconePath="/images/icons/files/powerpoint.png"},
                new FileType(){ 
                    Extention = ".ppt",
                    MimeType="application/vnd.ms-powerpoint" , 
                    IconePath="/images/icons/files/powerpoint.png"},
                new FileType(){
                    Extention = ".ppt",
                    MimeType="application/powerpoint" , 
                    IconePath="/images/icons/files/powerpoint.png"},
                new FileType(){ 
                    Extention = ".ppt",
                    MimeType="application/x-mspowerpoint" , 
                    IconePath="/images/icons/files/powerpoint.png"},

                    // excel
                    new FileType(){
                        Extention = ".xls",
                    MimeType="application/excel" , 
                    IconePath="/images/icons/files/xls.png"},                      
                    new FileType(){
                        Extention = ".xls",
                    MimeType="application/vnd.ms-excel" , 
                    IconePath="/images/icons/files/xls.png"},  
                    new FileType(){
                        Extention = ".xls",
                    MimeType="application/excel" , 
                    IconePath="/images/icons/files/xls.png"},                    
                    new FileType(){ 
                        Extention = ".xls",
                    MimeType="application/vnd.openxmlformats" , 
                    IconePath="/images/icons/files/xls.png"},
                    new FileType(){ 
                        Extention = ".xls",
                    MimeType="application/x-excel" , 
                    IconePath="/images/icons/files/xls.png"},
                    new FileType(){ 
                        Extention = ".xls",
                    MimeType="application/x-msexcel" , 
                    IconePath="/images/icons/files/xls.png"},     
                
                new FileType(){
                    Extention = ".xlsx",
                    MimeType="application/excel" , 
                    IconePath="/images/icons/files/xls.png"},                      
                new FileType(){
                    Extention = ".xlsx",
                    MimeType="application/vnd.ms-excel" , 
                    IconePath="/images/icons/files/xls.png"},  
                new FileType(){
                    Extention = ".xlsx",
                    MimeType="application/excel" , 
                    IconePath="/images/icons/files/xls.png"},                    
                new FileType(){ 
                    Extention = ".xlsx",
                    MimeType="application/vnd.openxmlformats" , 
                    IconePath="/images/icons/files/xls.png"},
                new FileType(){ 
                    Extention = ".xlsx",
                    MimeType="application/x-excel" , 
                    IconePath="/images/icons/files/xls.png"},
                new FileType(){ 
                    Extention = ".xlsx",
                    MimeType="application/x-msexcel" , 
                    IconePath="/images/icons/files/xls.png"},  
                    
                    // zip
                    new FileType(){ 
                        Extention = ".zip",
                    MimeType="application/x-compressed" , 
                    IconePath="/images/icons/files/zip.png"},
                    new FileType(){ 
                        Extention = ".zip",
                    MimeType="application/x-zip-compressed" , 
                    IconePath="/images/icons/files/zip.png"},                    
                    new FileType(){ 
                        Extention = ".zip",
                    MimeType="application/zip" , 
                    IconePath="/images/icons/files/zip.png"},    
                     new FileType(){ 
                        Extention = ".zip",
                    MimeType="multipart/x-zip" , 
                    IconePath="/images/icons/files/zip.png"},                  


                    // media
                    new FileType(){ 
                        Extention = ".avi",
                    MimeType="video audio" , 
                    IconePath="/images/icons/files/media.png"},


                    // pdf
                    new FileType(){ 
                        Extention = ".pdf",
                    MimeType="application/pdf" , 
                    IconePath="/images/icons/files/pdf.png"},

                    // rar
                    new FileType(){ 
                        Extention = ".rar",
                    MimeType="application/rar" , 
                    IconePath="/images/rar.png"}
            };
            }

        }

        public static string GetIcon(string memiType)
        {
            if(string.IsNullOrEmpty(memiType)) 
                return "/images/unknownfile.png";
            if (memiType.Contains("image")) return "/images/icons/files/image.png";

            var ext = MimeTypeMap.GetExtension(memiType);
            if(string.IsNullOrEmpty(ext))  
                return "/images/unknownfile.png";
            
            var url = Icons?.FirstOrDefault(a => a.Extention == ext)?.IconePath ?? "/images/unknownfile.png";
            return url;
        }

        public static string GetFileExt(string mimeFile)
        {
            return FilesIcons.Icons.Where(a => a.MimeType == mimeFile).Select(a => a.Extention).FirstOrDefault();
        }
        



    }
}