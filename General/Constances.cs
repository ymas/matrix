﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Resources;
using System.Threading;
using Microsoft.AspNetCore.Mvc.Rendering;
using Matrix.Resources;
using Matrix.Static;


namespace Matrix
{

    public static class Constants
    {
        private static readonly ResourceManager Rman = new ResourceManager(typeof(_Translate));
        private static readonly CultureInfo resourceCulture = Thread.CurrentThread.CurrentCulture;

        public static string _defaultLang = "ar_AE";

        public static readonly string OrganizationName = "Org name";
        
        
        
        public static class KpiUnit
        {
            public const string Percent = "percent";
            public const string Number = "number";

            public static SelectList SelectList(string selectedValue="")
            {
                var items = new List<SelectListItem>()
                {
                    new SelectListItem{ Value = Percent , Text = _Translate.percent , Selected = selectedValue.Equals(Percent)},
                    new SelectListItem{ Value = Number , Text = _Translate.Number , Selected = selectedValue.Equals(Number)},
                };
                return new SelectList(items, "Value" , "Text");
            }
        }
        
        
        public static class DepartmentType
        {
            public const string Department = "depatment";
            public const string Division = "division";
            public const string Unit = "unit";

            public static SelectList SelectList(string selectedValue="")
            {
                var items = new List<SelectListItem>
                {
                    new SelectListItem{ Value = Department , Text = "Department"},
                    new SelectListItem{ Value = Division , Text = "Division"},
                    new SelectListItem{ Value = Unit , Text = "Unit"}
                };
                return new SelectList(items, "Value" , "Text", selectedValue);
            }
        }
        

        public static class KpiResultCycle
        {
            public const string Quarterly = "Quarterly";
            public const string SemiAnnually = "SemiAnnually";
            public const string Annually = "Annually";
            
            public static SelectList SelectList(string selectedValue="")
            {
                var items = new List<SelectListItem>();
                foreach (var item in Enum.GetValues(typeof(KpiResultCycleEnum)))
                {
                    items.Add(new SelectListItem(){ Value = item.ToString() , Text = MyStatics.FindResByName(item.ToString()) , Selected = selectedValue.Equals(item.ToString())});
                }
                return new SelectList(items, "Value" , "Text");
            }
        }
        
        
        public static class KpiType
        {
            public const string Strategic = "strategic";
            public const string Operational = "operational";

          
            public static SelectList SelectList(string selectedValue="")
            {
                var items = new List<SelectListItem>()
                {
                    new SelectListItem(){ Value = Strategic , Text = _Translate.Strategic , Selected = selectedValue.Equals(Strategic)},
                    new SelectListItem(){ Value = Operational , Text = _Translate.Operational , Selected = selectedValue.Equals(Operational)},
                };
                return new SelectList(items, "Value" , "Text");
            }
        }


        public static class RubricRuleType
        {
            public const string Result = "result";
            public const string Enabler = "enabler";

            public static SelectList SelectList(string selectedValue="")
            {
                var items = new List<SelectListItem>()
                {
                    new SelectListItem(){ Value = Result , Text = _Translate.Results , Selected = selectedValue.Equals(Result)},
                    new SelectListItem(){ Value = Enabler , Text = _Translate.Enablers , Selected = selectedValue.Equals(Enabler)},
                };
                return new SelectList(items, "Value" , "Text");
            }
        }
        

        public static string FindResByName(string resName)
        {
            if(string.IsNullOrEmpty(resName))  return "-"; 
            var res =Rman.GetString(resName, Thread.CurrentThread.CurrentCulture);
            return string.IsNullOrEmpty(res) ? resName : res;
        }
    }

    public static class StatusColor
    {
        public static string Normal = "progress-bar-default";
        public static string Danger = "progress-bar-danger";
        public static string Warning = "progress-bar-warning";
        public static string success = "progress-bar-success";
    }
}