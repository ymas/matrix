﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Resources;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Matrix.Resources;
using Matrix.Services;

namespace Matrix
{
    public static class Utilties 
    {
        private static readonly ResourceManager Rman = new ResourceManager(typeof(_Translate));
        private static readonly MyHttpContext _MyHttp = new MyHttpContext(new HttpContextAccessor());
        public static string DefaultCookieName = ".AspNetCore.Culture";
        public static string PowerUserRoles = "Admin,Matrix";

        public static IEnumerable<T> Traverse<T>(this IEnumerable<T> items, 
            Func<T, IEnumerable<T>> childSelector)
        {
            var stack = new Stack<T>(items);
            while(stack.Any())
            {
                var next = stack.Pop();
                yield return next;
                foreach(var child in childSelector(next))
                    stack.Push(child);
            }
        }

        public static int NextSeq(int? currentTotal)
        {
            if (currentTotal == null) return 1;
            return ((int)currentTotal + 1);
        }
        

        public static async Task<byte[]> FormFileToBytes(IFormFile file)
        {
            if (file == null || file.Length <= 0) return null;
            var target = new MemoryStream();
            await  file.CopyToAsync(target);
            var data = target.ToArray();
            return data;
        }
        
        public static async Task<byte[]> LocalFileToBytes(string filePath)
        {
            if (string.IsNullOrEmpty(filePath)) return new byte[]{};
            var stream = File.OpenRead(filePath);
            var fileBytes= new byte[stream.Length];

            await  stream.ReadAsync(fileBytes, 0, fileBytes.Length);
            stream.Close();

            return fileBytes;
        }
        
        
        public static string AppDir()
        {
            var url = _MyHttp.AbsoluteUrl();
            var c = url.Split('/');
            if (c[3] == "Matrix")
                return "/Matrix/";
            else
                return "";
        }

        // cacluate percentage 
        public static float Percentage(float from, float to)
        {
            float res = 0;
            if (to > 0)
            {
                res = (float)Math.Round((double)((from / to) * 100), 2);
            }
            return res;
        }

        // get file name without extention
        public static string FileNameWithoutExtention(string FileName)
        {
            string extension = System.IO.Path.GetExtension(FileName);
            return FileName.Substring(0, FileName.Length - extension.Length);
        }

        // extract action and controller from the url
     
        public static Guid _guid(string id)
        {
            Guid ID = Guid.Empty;
            if (!string.IsNullOrEmpty(id))
            {
                ID = Guid.Parse(id);
            }

            return ID;
        }
        
        
          public static string FindResByName(string resName)
        {
            if(string.IsNullOrEmpty(resName))  return "-"; 
            var cLang = _MyHttp.GetCultureInfo;
            var res =Rman.GetString(resName, cLang);
            return string.IsNullOrEmpty(res) ? resName : res;
        }
        
        
       
        
        public static string TransableName(string ar, string en)
        {
            var trName = ar;
            var cLang = CultureInfo.CurrentUICulture.Name;
            if (cLang == null || cLang != "en-US") return trName;
            return !string.IsNullOrEmpty(en) ? en : trName;
        }

        
        public static async Task<byte[]> GetBytesFromUrlAsync(string url)
        {
            byte[] imageData = null;

            using (var wc = new HttpClient())
            {
                imageData = await wc.GetByteArrayAsync(url);
            }
            return imageData;
        }

       
        
        public static float GetPercent(float x, float of)
        {
            // x/of*100
            if (x == 0 || of == 0) return 0;
            var res = (float)Math.Round((double)(x/of*100),1);
            return res;
        }
        
        public static JsonResult GetResponse(int _success, object _extras, string _error)
        {
            var obj = new
            {
                success = _success,
                extras = _extras,
                error = _error
            };
            var res = new JsonResult(obj);
            return res;
        }

        
        public static string CalculateAge(DateTime? birthdate)
        {
            if (birthdate == null) return "-";
            // Save today's date.
            var today = DateTime.Today;
            // Calculate the age.
            var age = today.Year - birthdate.Value.Year;
            // Go back to the year the person was born in case of a leap year
            if (birthdate > today.AddYears(-age)) age--;
            return age.ToString();
        }
        
        
        public static byte[] PostedFileToBytes(IFormFile file)
        {
            if (file == null || file.Length <= 0) return null;
            var resultFile = new byte[]{};
            using (var stream = new MemoryStream())
            {
                file.CopyToAsync(stream);
                resultFile = stream.ToArray();
            }
            return resultFile;
        }

        public static string FileToBase64(byte[] file , DefaultEmptyFileName defaultEmptyFileName)
        {
            var d = _defaultEmptyFiles().FirstOrDefault(a => a.Key == defaultEmptyFileName.ToString()).Value;
            if (file != null && !(file?.Length <= 0))
                return String.Format("data:image/gif;base64,{0}", Convert.ToBase64String(file));

            if (d == null) return String.Empty;
            var filePath = "wwwroot/images/default/"+d;
            var binaryFile = File.ReadAllBytes(filePath);
            var base64 = Convert.ToBase64String(binaryFile);
            return String.Format("data:image/gif;base64,{0}", base64);

        }
        
        public static string FileToBase64(byte[] file , string defaultEmptyFileName)
        {
            var d = _defaultEmptyFiles().FirstOrDefault(a => a.Key == defaultEmptyFileName.ToString()).Value;
            if (file != null && !(file?.Length <= 0))
                return String.Format("data:image/gif;base64,{0}", Convert.ToBase64String(file));

            if (d == null) return String.Empty;
            var filePath = "wwwroot/images/default/"+d;
            var binaryFile = File.ReadAllBytes(filePath);
            var base64 = Convert.ToBase64String(binaryFile);
            return String.Format("data:image/gif;base64,{0}", base64);
        }
        
        private static IEnumerable<KeyValuePair<string, string>> _defaultEmptyFiles()
        {
            return new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("NoPhoto" ,"no-photo.jpg" ),
                new KeyValuePair<string, string>("NoImage" ,"no-image.jpg" ),
                new KeyValuePair<string, string>("NoReferee" ,"referee.png" ),
                new KeyValuePair<string, string>("NoClub" ,"no-club.png" ),
                new KeyValuePair<string, string>("NoPdf" ,"pdf.png" ),
            };
        }

       
    }

}