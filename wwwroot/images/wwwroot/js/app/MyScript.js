﻿// global Var

var pharses = {
    Cancel: "الغاء",
    // sweet alsert pharses
    SweetAlertTitle: "تأكيد عملية الحذف!",
    SweetAlertText: "تحذير ! سيتم حذف كافة المعلومات المرتبطة بهذه العملية!",
    SweetAlertconfirmButtonText: "نعم ، قم بالحذف!",
    SweerAlertDeleteBtn: "حــــذف!",
    SweerAlertDeleted: "تمت عملية الحذف بنجاح"
};


defaults = {
    color: '#424f63'
    , secondaryColor: '#dfdfdf'
    , jackColor: '#fff'
    , jackSecondaryColor: null
    , className: 'switchery'
    , disabled: false
    , disabledOpacity: 0.5
    , speed: '0.1s'
    , size: 'default'
};
var elems = Array.prototype.slice.call(document.querySelectorAll('.switchery'));
elems.forEach(function (html) {
    var switchery = new Switchery(html, defaults);
});

// lock & unlock page scroller
function LockScroller() {
    var scrollPosition = [
        self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
        self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
    ];
    var html = $('html'); // it would make more sense to apply this to body, but IE7 won't have that
    html.data('scroll-position', scrollPosition);
    html.data('previous-overflow', html.css('overflow'));
    html.css('overflow', 'hidden');
    window.scrollTo(scrollPosition[0], scrollPosition[1]);
}

function UnLockScroller() {
    var scrollPosition = [
        self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
        self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
    ];
    var html = $('html');
    scrollPosition = html.data('scroll-position');
    html.css('overflow', html.data('previous-overflow'));
    window.scrollTo(scrollPosition[0], scrollPosition[1])
}


$("img.colorbox").click(function () {
    jQuery('#loader').fadeOut();
    jQuery('#loader-wrapper').fadeOut();
    $.colorbox(
        {
            href: $(this).attr("src"), photo: true, onComplete() {
                ResiseColorbox()
            }
        }
    );
});

$("a.colorbox").click(function () {
   
    if ($(this).hasClass("file")) {
        $.colorbox(
            {
                href: $(this).attr("href"), width: "90%", height: "90%", iframe: true, photo: true, onComplete() {
                    ResiseColorbox()
                    jQuery('#loader').fadeOut();
                    jQuery('#loader-wrapper').fadeOut();
                }
            } 
    );
    } else {
        $.colorbox(
            {
                href: $(this).attr("href"), onComplete() {
                    $.colorbox.resize()
                    jQuery('#loader').fadeOut();
                    jQuery('#loader-wrapper').fadeOut();
                }
            }
        );
    }
    return false
});

// validation summer style
$(".validation-summary-errors").addClass("alert").addClass("alert-danger").addClass("alert-dismissable");


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


$.colorbox.settings.innerHeight = "70%";
$.colorbox.settings.innerWidth = "70%";

//color box resize 
function ResiseColorbox() {
    jQuery.colorbox.settings.maxWidth = '90%';
    jQuery.colorbox.settings.maxHeight = '90%';

    // ColorBox resize function
    var resizeTimer;

    function resizeColorBox() {
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            if (jQuery('#cboxOverlay').is(':visible')) {
                $.colorbox.resize({
                    width: '90%',
                    height: '90%'
                });
            }
        }, 300);
    }

    // Resize ColorBox when resizing window or changing mobile device orientation
    jQuery(window).resize(resizeColorBox);
    window.addEventListener("orientationchange", resizeColorBox, false);
}



(function ($) {

    $.fn.hasAttr = function (name) {
        return this.attr(name) !== undefined;
    };

   
    var getUrl = window.location;
    var baseUrl = getUrl.origin + "/";
    var langUrl = baseUrl + "lib/dataTables/dataTable.Arabic.json";
    var DefaualDir = "rtl";
    //console.log('coocke '+getCookie(".AspNetCore.Culture"));
    if ($("body").data("lang") === "en") {
        langUrl = baseUrl + "lib/dataTables/dataTable.English.json";
        DefaualDir = "ltr";
       
        pharses = {

            Cancel: "Cancel",

            // sweet alert pharses
            SweetAlertTitle: "Are you sure?",
            SweetAlertText: "Deleting this record will delete all related informations !",
            SweetAlertconfirmButtonText: "Yes, delete it!",
            SweerAlertDeleteBtn: "Deleted!",
            SweerAlertDeleted: "Done , Deleted successfully!"
        };

    }
    


    $(".delete").click(function () {
        jQuery('#loader').fadeOut();
        jQuery('#loader-wrapper').fadeOut();
        var _this = $(this);
        var _parentNode = _this.parents("tr");
        if (_this.hasAttr('data-SweetAlertTitle') && _this.attr("data-SweetAlertTitle").length > 0) {
            pharses.SweetAlertTitle = _this.attr("data-SweetAlertTitle");
        }
        if (_this.hasAttr('data-SweetAlertText') && _this.attr("data-SweetAlertText").length > 0) {
            pharses.SweetAlertText = _this.attr("data-SweetAlertText");
        }
        if (_this.hasAttr('data-SweetAlertconfirmButtonText') && _this.attr("data-SweetAlertconfirmButtonText").length > 0) {
            pharses.SweetAlertconfirmButtonText = _this.attr("data-SweetAlertconfirmButtonText");
        }
        if (_this.hasAttr('data-Cancel') && _this.attr("data-Cancel").length > 0) {
            pharses.Cancel = _this.attr("data-Cancel");
        }


        var url = $(this).data("url");
        swal({
            title: pharses.SweetAlertTitle,
            text: pharses.SweetAlertText,
            dangerMode: true,
            icon: 'warning',
            buttons: {
                cancel: {
                    text: pharses.Cancel,
                    value: false,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: pharses.SweetAlertconfirmButtonText,
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            }
        }).then((x) => {
            if (x) {
                $.ajax({
                    url: url,
                    type: "DELETE",
                    data: {},
                    dataType: "json",
                    success: function (res) {
                        if (res.success === 1) {
                            swal("Done!", "It was succesfully deleted!", "success");
                            _parentNode.hide();
                        } else {
                            swal("خطأ!", res.error, "error");
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("Error!", "حدث خطأ أثناء عملية الحذف .. الرجاء ابلاغ الدعم الفني", "error");
                    }
                });
            }
        });
        return false;
    });

    function onSucc(value) {
        console.log(value);
        return value;
    }

    $.extend(true, $.fn.dataTable.defaults, {
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "الكل"]],
        "dom": "<'row'<'col-md-12'<'pull-right'f><'pull-left'l>>>t<'row'<'col-md-12'<'pull-left'i><'pull-right'p>>>",
        "searching": true,
        "ordering": false,
        "paging": true,
        "stateSave": true,
        "language": {
            url: langUrl
        },"draw":function(){
            $('[data-toggle="tooltip"]').tooltip();
        }
    });

    $(".dt").dataTable();

    // apply select2 to each dropdownlist has class select
    var Select2options =
        {
            allowClear: true,
            dir: DefaualDir,
            placeholder: {
                id: "",
                placeholder: "..."
            }
        };


    $(".select").select2(Select2options);

    $(".select-empty").select2(Select2options).val('').trigger('change');





    // button action
    $("button[data-url]").click(function () {
        if (!$(this).hasClass("delete")) {
            window.location.href = $(this).data("url");
        }
    });


    //table action div wrap contents
    $(".actions").parent("td").css("width", "5%");

    $("li.openable>a").click(function () {
        jQuery('#loader').fadeOut();
        jQuery('#loader-wrapper').fadeOut();
        $.cookie("activeMenu", $(this).parent("li").index());
    });


    // send registration form 
    $(".sendRegistrationForm").on("click", function () {
        jQuery('#loader').fadeOut();
        jQuery('#loader-wrapper').fadeOut();
        var statusTD = $(this).parents("tr").find("td.orderStatus");
        swal({
            title: "تأكيد ارسال استمارة التسجيل",
            text: "بعد ارسال هذه الاستمارة لن يكون بمقدورك تعديل بيانات الإستمارة",
            dangerMode: true,
            icon: 'warning',
            buttons: {
                cancel: {
                    text: "الغاء",
                    value: false,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "تأكيد الإرسال !",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: true
                }
            }
        }).then((x) => {
            if (x) {
                $.ajax({
                    url: '/RegisterPlayers/SendOrder/' + $(this).attr("id"),
                    type: "GET",
                    data: {},
                    dataType: "json",
                    success: function (res) {
                        //console.log(res);
                        console.log(statusTD);
                        if (res.success === 1) {
                            swal("تم الإرسال!", "تم تحويل الطلب إلى الإتحاد وسيتم اخطاركم بقرار اللجنه", "success");
                            statusTD.html(res.extras.newStatus);
                        } else {
                            swal("خطأ!!", res.error, "error");
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        swal("خطأ!!", "لم تكتمل عملية الارسال الرجاء التواصل بالدعم الفني لحل المشكلة", "error");
                    }
                });
            }
        });
        return false;
    });

    $("#CheckAll").click(function () {
        if ($(this).is(":checked")) {
            $(this).parents("form").find("button[type='submit']").prop("disabled", false);

            $(".check_all_item:not(:checked)").each(function () {
                $(this).prop("checked", true)
            });

        } else {
            $(".check_all_item:checked").each(function () {
                $(this).prop("checked", false)
            });
            if ($(".check_all_item:checked").length === 0) {
                $(this).parents("form").find("button[type='submit']").prop("disabled", true);
            }
        }
    });

    $(".check_all_item").click(function () {
            $(this).parents("form").find("button[type='submit']").prop("disabled", false);

    });
    
    
    
    

})(jQuery);




function DeleteMe(item) {
    jQuery('#loader').fadeOut();
    jQuery('#loader-wrapper').fadeOut();
    var _this = $(item);
    var _parentNode = _this.parents("tr");
    if (_this.hasAttr('data-SweetAlertTitle') && _this.attr("data-SweetAlertTitle").length > 0) {
        pharses.SweetAlertTitle = _this.attr("data-SweetAlertTitle");
    }
    if (_this.hasAttr('data-SweetAlertText') && _this.attr("data-SweetAlertText").length > 0) {
        pharses.SweetAlertText = _this.attr("data-SweetAlertText");
    }
    if (_this.hasAttr('data-SweetAlertconfirmButtonText') && _this.attr("data-SweetAlertconfirmButtonText").length > 0) {
        pharses.SweetAlertconfirmButtonText = _this.attr("data-SweetAlertconfirmButtonText");
    }
    if (_this.hasAttr('data-Cancel') && _this.attr("data-Cancel").length > 0) {
        pharses.Cancel = _this.attr("data-Cancel");
    }


    var url = $(this).data("url");
    swal({
        title: pharses.SweetAlertTitle,
        text: pharses.SweetAlertText,
        dangerMode: true,
        icon: 'warning',
        buttons: {
            cancel: {
                text: pharses.Cancel,
                value: false,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: pharses.SweetAlertconfirmButtonText,
                value: true,
                visible: true,
                className: "",
                closeModal: true
            }
        }
    }.then((x) => {
        if (x) {
            $.ajax({
                url: url,
                type: "DELETE",
                data: {},
                dataType: "json",
                success: function (res) {
                    if (res.success === 1) {
                        swal("Done!", "It was succesfully deleted!", "success");
                        _parentNode.hide();
                    } else {
                        swal("خطأ!", res.error, "error");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    swal("Error!", "حدث خطأ أثناء عملية الحذف .. الرجاء ابلاغ الدعم الفني", "error");
                }
            });
        }
    }));
    return false;
}

    