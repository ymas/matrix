(function ($) {

    // ++++++++++++++++++++  TransferPlayers/Create +++++++++++++++++///
    $("#PlayerId").select2({
        templateResult:itemResult,
        templateSelection : ItemSelected
    });

    function ItemSelected (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var $opt = $(
            '<span class="select2-selected-item-image"><img  height="70" src="/Download/PlayerPhoto/'+opt.id+'" class="avatar img-responsive img-circle" /> ' + $(opt.element).text() + '</span>'
        );
        return $opt;
    }

    function itemResult (opt) {
        if (!opt.id) {
            return opt.text;
        }
        var $opt = $(
            '<span class="select2-selected-item-image" style="line-height: 70px; "><img  height="70" src="/Download/PlayerPhoto/'+opt.id+'" class="avatar img-responsive img-circle" /> ' + $(opt.element).text() + '</span>'
        );
        return $opt;
    }
})(jQuery);