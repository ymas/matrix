let page = function () {

    // $("#AddNewAttachment").click(function() {
    //     //     $.colorbox({
    //     //         href: '@Url.Action("Create", "Attachments", new {id = Model.Id})',
    //     //         iframe: false,
    //     //         width: '50%',
    //     //         height: '550',
    //     //         onLoad: function() {
    //     //             $("#colorbox").removeAttr("tabindex");
    //     //         },
    //     //         onComplete: function() {
    //     //             document.LoadGlobalConfigurations();
    //     //         }
    //     //     });
    //     //     return false;
    //     // });

    /**
     * @return {boolean}
     */
    let CreateNewAttachment = function (id) {
        $.colorbox({
            href: '/Attachments/Create/' + id,
            iframe: false,
            width: '50%',
            height: '550',
            onLoad: function () {
                $("#colorbox").removeAttr("tabindex");
            },
            onComplete: function () {
                document.LoadGlobalConfigurations();
            }
        });
        return false;
    };

    return {
        init: function (id) {
            $("#AddNewAttachment").click(function() {
               return  CreateNewAttachment(id);
            });
            console.log(id);
        }
    }
}(); // jquery

