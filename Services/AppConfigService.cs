using System;
using Microsoft.Extensions.Configuration;

namespace Matrix.Services
{
    public  class AppConfigService 
    {
        public  AppConfigServiceEmail Email { get; set; }
        public  object Files { get; }

        public AppConfigService(IConfiguration configuration)
        {
            Email = new AppConfigServiceEmail()
            {
                FromEmail = configuration["Email:FromEmail"],
                EntityDomain = configuration["Email:EntityDomain"]
            };
            Files = new
            {
                AcceptedFileTypes = configuration["AcceptedFileTypes"]
            };
        }
    }

     public class AppConfigServiceEmail
     {
        public string FromEmail { get; set; }
        public string EntityDomain { get; set; }
     }
}