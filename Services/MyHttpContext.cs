﻿using System.Globalization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Localization;

namespace Matrix.Services
{
    
    public class MyHttpContext
    {
        private readonly IHttpContextAccessor _http;

        public MyHttpContext(IHttpContextAccessor httpContextAccessor)
        {
            _http = httpContextAccessor;
        }

        public CultureInfo GetCultureInfo => _http.HttpContext.Features.Get<IRequestCultureFeature>().RequestCulture.Culture;
        public HttpContext GetHttpContext()
        {
            return _http.HttpContext;
        }

        public string AbsoluteUrl()
        {
            return _http.HttpContext.Request.GetDisplayUrl();
        }
    }
}