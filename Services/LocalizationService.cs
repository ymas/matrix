﻿using System;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Localization;
using Matrix.Resources;

namespace Matrix.Services
{
    public class LocalizationService
    {
        private readonly IStringLocalizer _localizer;
        private readonly MyHttpContext _httpContext;
        public bool IsEn = false;

        public LocalizationService(IStringLocalizerFactory factory, MyHttpContext httpContext)
        {
            _httpContext = httpContext;
            var type = typeof(_Translate);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _localizer = factory.Create("_Translate", assemblyName.Name);
        }
 
        public LocalizedString GetLocalizedHtmlString(string key)
        {
            return _localizer[key];
        }

        private void GetCurrentLang()
        {
            const string pattern = "(?<=uic=).*$";
            try
            {
                var uic = Regex.Match(_httpContext.GetHttpContext().Request.Cookies[".AspNetCore.Culture"], pattern);
                if (uic.Value.ToLower().Contains("us"))
                {
                    IsEn = true;
                }
            } catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
