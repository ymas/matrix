using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Matrix.Dtos.Users;
using Matrix.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Configuration;
using Ymas.Extensions;

namespace Matrix.Services
{
    public class EmailService : IEmailSender
    {
        private readonly string _smtpServer;
        private readonly string _username;
        private readonly string _password;
        private readonly string _port;
        private readonly string _enableSsl;
        private readonly string _defaultSenderEmail;
        private readonly string _templatesPath;
        public enum TemplateTypes
        {
            NewUser,
            ChangePassword,
            NewTask,
            TaskReminder,
            NewTeamMember,
            CancelTeamMember,
            Empty
        }
        public EmailService(IConfiguration configuration , IHostingEnvironment environment)
        {
            _smtpServer = configuration["Email:Host"];
            _username = configuration["Email:Credentials:Username"];
            _password = configuration["Email:Credentials:Password"];
            _port = configuration["Email:SmtpPort"];
            _enableSsl = configuration["Email:EnableSsl"];
            _defaultSenderEmail = configuration["Email:FromEmail"];
            _templatesPath = environment.WebRootPath + "/Templates/";

        }

        public async Task SendEmailAsync(EmailSenderModel msg)
        {
            var client = new SmtpClient(_smtpServer , _port.ToInt())
            {
                UseDefaultCredentials = false,
                EnableSsl = bool.Parse(_enableSsl),
                Credentials = new NetworkCredential(_username, _password)
            };

            var mailMessage = new MailMessage
            {
                From = new MailAddress(string.IsNullOrEmpty(msg.FromEmail) ? _defaultSenderEmail : msg.FromEmail),
                Body = msg.Body,
                Subject = msg.Subject,
                IsBodyHtml = true,
                Sender = new MailAddress(string.IsNullOrEmpty(msg.FromEmail) ? _defaultSenderEmail : msg.FromEmail , "برنامج منظومة التميز الحكومي")
            };
            
            
            foreach (var to in msg.ToEmails)
            {
                mailMessage.To.Add(to);
            }

            if (msg.Attachments !=null && msg.Attachments.Any())
            {
                foreach (var a in msg.Attachments)
                {
                    mailMessage.Attachments.Add(a);
                }
            }

            try
            {
                await client.SendMailAsync(mailMessage);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            } 
        }

       
        
        public string CreateBody(TemplateTypes messageType , EmailBodyVariables variables,  bool isEn=false)
        {
            var body = string.Empty;
            
                var lang = isEn ? "en" : "ar";
                var path = $"{_templatesPath}{messageType}.{lang}.html";
                using (var streamReader = new StreamReader(path))
                {
                    body = streamReader.ReadToEnd();
                }
           
            body = body.Replace("{firstName}", isEn ? variables.NameEn : variables.NameAr);
            body = body.Replace("{userName}", variables.UserName);
            body = body.Replace("{password}", variables.Password);
            body = body.Replace("{loginUrl}", variables.LoginUrl);
            body = body.Replace("{myTaskUrl}", variables.MyTasksUrl);
            body = body.Replace("{startTime}", variables.StartTime);
            body = body.Replace("{deadline}", variables.Deadline);
            body = body.Replace("{task}", variables.Task);
            body = body.Replace("{totalRemainDays}", variables.TotalRemainDays);
            body = body.Replace("{message}", variables.Message);
            body = body.Replace("{assignBy}", variables.AssignBy);
            return body;
        }
    }
}