﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Matrix.Data;
using Matrix.Services;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Microsoft.AspNetCore.Mvc.Razor;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Matrix.Data.DomainClasses;
using Matrix.Interfaces;
using Matrix.Models.IdentityModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Matrix.Resources;
using DataTables.AspNet.AspNetCore;
using Matrix.MiddleWare;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.CookiePolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection.Extensions;


namespace Matrix
{
    public class Startup
    {
        private IConfiguration Configuration { get; }
        private bool IsDevelopingEnvironment { get; set; }

        public Startup(IHostingEnvironment hostingEnvironment)
        {
            IsDevelopingEnvironment = hostingEnvironment.IsDevelopment();
            Configuration = new ConfigurationBuilder()
                .SetBasePath(hostingEnvironment.ContentRootPath)
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{hostingEnvironment.EnvironmentName.ToLower()}.json", true)
                .AddEnvironmentVariables()
                .Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // email sender 
            services.AddTransient<IEmailSender, EmailService>();


            // frequents
            services.AddScoped(typeof(AppConfigService));
            
            //localization
            services.AddScoped(typeof(LocalizationService));

            //data tables
            services.RegisterDataTables();

            // connection string
            //Console.WriteLine(Configuration.GetConnectionString("DefaultConnection"));


            // HttpContext
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // db context & lazy loading
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseLazyLoadingProxies()
                    .UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // identity cookies
            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(365);
                // the path to /Account/Login.
                options.LoginPath = "/Account/Login";
                options.LogoutPath = "/Account/Logout";
                // If the AccessDeniedPath isn't set, ASP.NET Core defaults 
                // the path to /Account/AccessDenied.
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.SlidingExpiration = true;
                options.Cookie.SecurePolicy = IsDevelopingEnvironment ? CookieSecurePolicy.SameAsRequest :  CookieSecurePolicy.Always;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie();
            
            // identity options
            services.AddIdentity<ApplicationUser, Role>(options =>
                {
                    options.Password.RequiredLength = Configuration.GetValue<int>("Identity:PasswordRequiredLength");
                    options.Password.RequireLowercase = Configuration.GetValue<bool>("Identity:RequireLowercase");
                    options.Password.RequireUppercase = Configuration.GetValue<bool>("Identity:RequireUppercase");
                    options.Password.RequireNonAlphanumeric =
                        Configuration.GetValue<bool>("Identity:RequireNonAlphanumeric");
                    options.Password.RequireDigit = Configuration.GetValue<bool>("Identity:RequireDigit");

                    // Lockout settings
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                    options.Lockout.MaxFailedAccessAttempts = 5;
                    options.Lockout.AllowedForNewUsers = false;

                    // User settings
                    options.User.RequireUniqueEmail = true;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            
            services.AddSession(options =>
            {
                options.Cookie.SecurePolicy = IsDevelopingEnvironment ? CookieSecurePolicy.SameAsRequest :  CookieSecurePolicy.Always;
                options.Cookie.HttpOnly = true;
            });

            //add custom claims to user identity
            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, AppClaimsPrincipalFactory>();

            // seeder
            services.AddScoped<Seeder>();
          
            // set custom cookie name
            services.AddAntiforgery(opts => opts.Cookie.HttpOnly = true);
            services.AddAntiforgery(opts => opts.Cookie.SecurePolicy = IsDevelopingEnvironment ? CookieSecurePolicy.SameAsRequest : CookieSecurePolicy.Always);  


            // localization
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.AddHttpsRedirection(options =>  
            {  
                options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;  
                //options.HttpsPort = 44344;  
            });  
            
            // secure TempDate cookies
            services.Configure<CookieTempDataProviderOptions>(options => options.Cookie.HttpOnly=true);            
            services.Configure<CookieTempDataProviderOptions>(options => options.Cookie.SecurePolicy=IsDevelopingEnvironment ? CookieSecurePolicy.SameAsRequest :  CookieSecurePolicy.Always);

            // correct way to register httpContext service dotnet 2.1 +
            services.AddHttpContextAccessor();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();  
            
            services.AddMvc(config =>
                {
                    var policy = new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .Build();
                    config.Filters.Add(new AuthorizeFilter(policy));
                })
                .AddViewLocalization(
                    LanguageViewLocationExpanderFormat.Suffix,
                    options => options.ResourcesPath = "Resources"
                )
                .AddDataAnnotationsLocalization(options =>
                {
                    options.DataAnnotationLocalizerProvider = (type, factory) =>
                    {
                        var assemblyName = new AssemblyName(typeof(_Translate).GetTypeInfo().Assembly.FullName);
                        return factory.Create("_Translate", assemblyName.Name);
                    };
                });

            // HTTP Strict Transport Security (HSTS)
            services.AddHsts( options=>
            {
                options.MaxAge = TimeSpan.FromDays(30);
                options.IncludeSubDomains = false;  
            });

            // setup default localization
            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en-US"),
                        new CultureInfo("ar-AE")
                    };

                    options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "ar-AE");
                    options.SupportedCultures = new[] {new CultureInfo("en-US")};
                    options.SupportedUICultures = supportedCultures;
                    options.RequestCultureProviders.Insert(0, new QueryStringRequestCultureProvider());
                    services.AddAntiforgery(opts => opts.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest);  
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,  IAntiforgery antiforgery, IHostingEnvironment env, IServiceProvider serviceProvider,
            ILoggerFactory loggerFactory)
        {
            // auto db migration & seeding
            using (var services = serviceProvider.CreateScope())
            {
                var db = services.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                db.Database.Migrate();

                // seeding things here
                var seeder = services.ServiceProvider.GetService<Seeder>();

                if (Configuration.GetValue<bool>("Data:Seed:All"))
                {
                    seeder.SeedAll().Wait();
                }
                else
                {
                    if (Configuration.GetValue<bool>("Data:Seed:Roles"))
                    {
                        seeder.SeedRoles().Wait();
                    }

                    if (Configuration.GetValue<bool>("Data:Seed:Admin"))
                    {
                        seeder.SeedAdmin().Wait();
                    }
                }
            }

            //env 
            if (env.IsDevelopment())
            {
                // logging
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                loggerFactory.AddDebug();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.HttpsRedirectMiddleWare();
            }
        
           
            //Set X-FRAME-OPTIONS :  protecting against click jacking attempts
            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add("X-Frame-Options", "SAMEORIGIN");
                await next();
            });
            
            //static files
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseHsts();
            
            // localization
            app.UseRequestLocalization(LocalizationOptions());
            app.UseCookiePolicy(new CookiePolicyOptions()
            {
                Secure = CookieSecurePolicy.SameAsRequest , HttpOnly = HttpOnlyPolicy.Always
            });
           
            app.UseStatusCodePagesWithReExecute("/Home/Error", "?statusCode={0}");
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

        private RequestLocalizationOptions LocalizationOptions()
        {
            var supportedCultures = new List<CultureInfo>
            {
                new CultureInfo("en-US"),
                new CultureInfo("ar-AE")
            };
            var options = new RequestLocalizationOptions();
            options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "ar-AE");
            options.SupportedCultures = new[] {new CultureInfo("en-US")};
            options.SupportedUICultures = supportedCultures;
            options.RequestCultureProviders.Insert(0, new QueryStringRequestCultureProvider());
            return options;
        }
    }
}