using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;

namespace Matrix.ActionFilters
{
    public class DetectBrowserFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.HttpContext.Request.Headers["User-Agent"].ToString().Contains("MSIE") ||
                context.HttpContext.Request.Headers["User-Agent"].ToString().Contains("Trident"))
            {
                context.Result = new RedirectToRouteResult(
                    new RouteValueDictionary
                    {
                        {"controller", "Home"},
                        {"action", "NotSupportedBrowser"}
                    }
                );
            }            
        }
    }
}