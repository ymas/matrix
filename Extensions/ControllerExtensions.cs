using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using Matrix;
using Matrix.Data.DomainClasses;
using Matrix.Extensions;
using Ymas.Extensions;

namespace Microsoft.AspNetCore.Mvc
{
    public static class ControllerExtensions
    {
        public static bool IsEn(this ControllerBase controller)
        {
            var cLang = CultureInfo.CurrentUICulture.Name;
            if (cLang == null || cLang != "en-US") return false;
            return true;
        }
        
        public static Guid? GetUserId(this ControllerBase controller)
        {
            var identity = controller.HttpContext?.User?.Identity ?? null;
            var userIdString =  ((ClaimsIdentity) identity)?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            return userIdString?.ToGuid();
        }
    
    }
}