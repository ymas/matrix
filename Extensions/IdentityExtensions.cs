﻿using System;
using System.Security.Claims;
using System.Security.Principal;
using Ymas.Extensions;

namespace Matrix.Extensions
{
    public static class IdentityExtensions
    {
            public static Guid GetEmployeeId(this IIdentity identity)
            {
                if (identity == null)
                    throw new ArgumentNullException(nameof(identity));
                var claim = ((ClaimsIdentity)identity).FindFirst("EmployeeNo");
                // Test for null to avoid issues during local testing
                return claim?.Value.ToGuid() ?? Guid.Empty;
            }
        
        public static string GetArabicName(this IIdentity identity)
        {
            if (identity == null)
                throw new ArgumentNullException(nameof(identity));
            var claim = ((ClaimsIdentity)identity).FindFirst("ArabicName");
            return claim?.Value ?? "زائر";
        }
        
        public static string GetEnglishName(this IIdentity identity)
        {
            if (identity == null)
                throw new ArgumentNullException(nameof(identity));
            var claim = ((ClaimsIdentity)identity).FindFirst("EnglishName");
            // Test for null to avoid issues during local testing
            return claim?.Value ?? "Guest";
        }
        
        public static string GetTeamsIds(this IIdentity identity)
        {
            if (identity == null)
                throw new ArgumentNullException(nameof(identity));
            var claim = ((ClaimsIdentity)identity).FindFirst("TeamsIds");
            // Test for null to avoid issues during local testing
            return claim?.Value ?? null;
        }
        public static bool IsEnglish(this IIdentity identity)
        {
            if (identity == null)
                throw new ArgumentNullException(nameof(identity));
            var claim = ((ClaimsIdentity)identity).FindFirst("IsEnglishLanguage");
            // Test for null to avoid issues during local testing
            return  bool.Parse(claim?.Value);
        }

         public static string GetUserId(this IIdentity identity)
          {
                if (identity == null)
                    throw new ArgumentNullException(nameof(identity));
                return ((ClaimsIdentity)identity).FindFirst(ClaimTypes.NameIdentifier)?.Value;
          }

        public static bool HasClaim(this IIdentity identity, string clameName)
        {
            if (identity == null)
                throw new ArgumentNullException(nameof(identity));            
            var claim = ((ClaimsIdentity)identity).FindFirst(clameName);
            if (claim != null) return true;
            return false;
        }
        
        
        public static bool IsSuperAdmin(this IIdentity identity)
        {
            if (identity == null)
                throw new ArgumentNullException(nameof(identity));
            if (((ClaimsIdentity) identity).FindFirst(ClaimTypes.Role)?.Value == "Admin")
            {
                return true;
            }

            return false;
        }
    }
}