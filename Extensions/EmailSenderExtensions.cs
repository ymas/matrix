using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Matrix.Interfaces;

namespace Matrix.Extensions
{
    public static class EmailSenderExtensions
    {
        public static Task SendEmailConfirmationAsync(this IEmailSender emailSender, string email, string link)
        {

            return emailSender.SendEmailAsync(new EmailSenderModel()
            {
                ToEmails = new[] {email},
                Body =
                    $"Please confirm your account by clicking this link: <a href='{HtmlEncoder.Default.Encode(link)}'>link</a>",
                Subject = "Confirm your email"
            });
        }
    }
}
