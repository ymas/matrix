using System;
using System.Text.RegularExpressions;
using Ymas.Extensions;

namespace Matrix.Extensions
{
    public static class StringTempExtensions
    {
        public static int? ExtractNumbers(this string str)
        {
            var match = Regex.Match(str, @"\d+");
            return match.Success ? match.Value.ToInt() as int? : null;
        }
        
        public static double RoundUp(this double input, int decimals=2)
        {
            return Math.Round(input, decimals);
        }
        
       
    }

    
}