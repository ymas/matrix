using System;

namespace Matrix.Extensions
{
    public static class DoubleExtension
    {
        public static double Round(this double s)
        {
            return Math.Round(s, 2);
        }
        
        public static double Round(this double? s)
        {
            return s== null ? 0 :  Math.Round(s.Value, 2);
        }
    }
}