using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Ymas.Extensions;

namespace Matrix.Helpers.DataTablesModelBinder
{

public class DtModelBinder: IModelBinder
    {
       
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var request = bindingContext.HttpContext?.Request.Form;
            if (request == null)
            {
                 throw new ArgumentNullException(nameof(request));;
            }
            if (bindingContext == null) throw new ArgumentNullException(nameof(bindingContext));

            // Retrieve request data
            var draw =  request["draw"].ToString().ToInt();
            var start = request["start"].ToString().ToInt();
            var length = request["length"].ToString().ToInt();
            // Search
            var search = new DtSearch
            {
                Value = request["search[value]"],
                Regex = Convert.ToBoolean(request["search[regex]"])
            };
            // Order
            var o = 0;
            var order = new List<DtOrder>();
            while (!string.IsNullOrEmpty(request["order[" + o + "][column]"].ToString()))
            {
                order.Add(new DtOrder
                {
                    Column = Convert.ToInt32(request["order[" + o + "][column]"]),
                    Dir = request["order[" + o + "][dir]"]
                });
                o++;
            }
            
            // Columns
            var c = 0;
            var columns = new List<DtColumn>();
            while (!string.IsNullOrEmpty(request["columns[" + c + "][name]"].ToString()))
            {
                columns.Add(new DtColumn
                {
                    Data = request["columns[" + c + "][data]"],
                    Name = request["columns[" + c + "][name]"],
                    Orderable = Convert.ToBoolean(request["columns[" + c + "][orderable]"]),
                    Searchable = Convert.ToBoolean(request["columns[" + c + "][searchable]"]),
                    Search = new DtSearch
                    {
                        Value = request["columns[" + c + "][search][value]"],
                        Regex = Convert.ToBoolean(request["columns[" + c + "][search][regex]"])
                    }
                });
                c++;
            }
            
            var valueProviderResult =
                bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            
            if (valueProviderResult == ValueProviderResult.None)
               // return Task.CompletedTask;
            
            bindingContext.ModelState.SetModelValue(
                bindingContext.ModelName, valueProviderResult);
           
 
            bindingContext.Result = ModelBindingResult.Success( new DataTableModel
            {
                Draw = draw,
                Start = start,
                Length = length,
                Search = search,
                Order = order,
                Columns = columns
            });
            return Task.CompletedTask;
        }
    }
    
    
[ModelBinder(typeof(DtModelBinder))]
public class DataTableModel
{
    
    /// <summary>
    /// Draw counter. This is used by DataTables to ensure that the Ajax returns from 
    /// server-side processing requests are drawn in sequence by DataTables 
    /// </summary>
    public int Draw { get; set; }

    /// <summary>
    /// Paging first record indicator. This is the start point in the current data set 
    /// (0 index based - i.e. 0 is the first record)
    /// </summary>
    public int Start { get; set; } 

    /// <summary>
    /// Number of records that the table can display in the current draw. It is expected
    /// that the number of records returned will be equal to this number, unless the 
    /// server has fewer records to return. Note that this can be -1 to indicate that 
    /// all records should be returned (although that negates any benefits of 
    /// server-side processing!)
    /// </summary>
    public int Length { get; set; }

    /// <summary>
    /// Global Search for the table
    /// </summary>
    public DtSearch Search { get; set; }

    /// <summary>
    /// Collection of all column indexes and their sort directions
    /// </summary>
    public IEnumerable<DtOrder> Order { get; set; }

    /// <summary>
    /// Collection of all columns in the table
    /// </summary>
    public IEnumerable<DtColumn> Columns { get; set; }
}

/// <summary>
/// Represents search values entered into the table
/// </summary>
public sealed class DtSearch
{
    /// <summary>
    /// Global search value. To be applied to all columns which have searchable as true
    /// </summary>
    public string Value { get; set; }

    /// <summary>
    /// true if the global filter should be treated as a regular expression for advanced 
    /// searching, false otherwise. Note that normally server-side processing scripts 
    /// will not perform regular expression searching for performance reasons on large 
    /// data sets, but it is technically possible and at the discretion of your script
    /// </summary>
    public bool Regex { get; set; }
}

/// <summary>
/// Represents a column and it's order direction
/// </summary>
public sealed class DtOrder
{
    /// <summary>
    /// Column to which ordering should be applied. This is an index reference to the 
    /// columns array of information that is also submitted to the server
    /// </summary>
    public int Column { get; set; }

    /// <summary>
    /// Ordering direction for this column. It will be asc or desc to indicate ascending
    /// ordering or descending ordering, respectively
    /// </summary>
    public string Dir { get; set; }
}

/// <summary>
/// Represents an individual column in the table
/// </summary>
public sealed class DtColumn
{
    /// <summary>
    /// Column's data source
    /// </summary>
    public string Data { get; set; }

    /// <summary>
    /// Column's name
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Flag to indicate if this column is orderable (true) or not (false)
    /// </summary>
    public bool Orderable { get; set; }

    /// <summary>
    /// Flag to indicate if this column is searchable (true) or not (false)
    /// </summary>
    public bool Searchable { get; set; }

    /// <summary>
    /// Search to apply to this specific column.
    /// </summary>
    public DtSearch Search { get; set; }
}
}