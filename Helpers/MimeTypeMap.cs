﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Matrix.Helpers
{
    public static class MimeTypeMap
    {

        private static readonly List<KeyValue> Mappings =
          new List<KeyValue>(){            
                
                new KeyValue (".bmp", "image/bmp"),
              new KeyValue (".png", "image/png"),
              
                new KeyValue (".doc", "application/msword"),
                new KeyValue (".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
                new KeyValue (".doc", "application/vnd.ms-word.template.macroEnabled.12"),

             
              new KeyValue (".pdf", "application/pdf"),
                
              new KeyValue (".ppt", "application/vnd.ms-powerpoint.addin.macroEnabled.12"),
                new KeyValue (".ppt", "image/x-portable-pixmap"),
                new KeyValue (".ppt", "application/vnd.ms-powerpoint"),
                new KeyValue (".ppt", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"),
                new KeyValue (".ppt", "application/vnd.openxmlformats-officedocument.presentationml.slideshow"),
                new KeyValue (".ppt", "application/vnd.ms-powerpoint"),
                new KeyValue (".ppt", "application/vnd.openxmlformats-officedocument.presentationml.template"),
                new KeyValue (".ppt", "application/vnd.ms-powerpoint.template.macroEnabled.12"),
                new KeyValue (".ppt", "application/vnd.ms-powerpoint.presentation.macroEnabled.12"),
                new KeyValue (".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation"),
                
              new KeyValue (".svg", "image/svg+xml"),
                new KeyValue (".tex", "application/x-tex"),
                
              new KeyValue (".xls", "application/vnd.ms-excel"),
                new KeyValue (".xls", "application/vnd.ms-excel.addin.macroEnabled.12"),
                new KeyValue (".xls", "application/vnd.ms-excel"),
                new KeyValue (".xls", "application/vnd.openxmlformats"),
                new KeyValue (".xls", "application/vnd.ms-excel.sheet.binary.macroEnabled.12"),
                new KeyValue (".xls", "application/vnd.ms-excel.sheet.macroEnabled.12"),
                new KeyValue (".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"),
                new KeyValue (".xlt", "application/vnd.ms-excel"),
                new KeyValue (".xls", "application/vnd.ms-excel.template.macroEnabled.12"),
                new KeyValue (".xls", "application/vnd.openxmlformats-officedocument.spreadsheetml.template"),
                
              new KeyValue (".zip", "application/zip"),

                
                };


        public static string GetMimeType(string extension)
        {
            if (extension == null)
            {
                throw new ArgumentNullException("extension");
            }

            if (!extension.StartsWith("."))
            {
                extension = "." + extension;
            }

            return Mappings.FirstOrDefault(a=>a.Key.Equals(extension))?.Value ??  "application/octet-stream";
        }

        public static string GetExtension(string mimeType)
        {
             return GetExtension(mimeType, false);
        }
        
        public static string GetExtension(string mimeType, bool throwErrorIfNotFound)
        {
            if (mimeType == null)
            {
                throw new ArgumentNullException("mimeType");
            }

            if (mimeType.StartsWith("."))
            {
                throw new ArgumentException("Requested mime type is not valid: " + mimeType);
            }

            return Mappings.FirstOrDefault(a=>a.Value.Equals(mimeType))?.Key ??   string.Empty;   ;
            
        }
    }
}
