﻿using System.Net.Mail;
using System.Threading.Tasks;
using Matrix.Services;

namespace Matrix.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailAsync(EmailSenderModel email);

        string CreateBody(EmailService.TemplateTypes messageType, EmailBodyVariables variables,  bool isEn = false);
    }

    public  class EmailSenderModel
    {
        public string Body { get; set; }
        public string Subject { get; set; }
        public string FromEmail { get; set; }
        public string[] ToEmails { get; set; }
        public Attachment[] Attachments { get; set; }
    }

    public class EmailBodyVariables
    {
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string LoginUrl { get; set; }
        public string MyTasksUrl { get; set;}
        public string StartTime { get; set;}
        public string Deadline { get; set;}
        public string Task { get; set;}
        public string TotalRemainDays { get; set;}
        public string Message { get; set;}
        public string AssignBy { get; set;}
    }
}
