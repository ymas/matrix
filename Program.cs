﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace Matrix
{
    public class Program
    {
        public static void Main(string[] args)
        {
           
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                
                .UseStartup<Startup>()
                .UseUrls("https://localhost:5000")
                .Build();
    }
}
