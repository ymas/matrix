using Matrix.Data;
using Microsoft.AspNetCore.Mvc;

namespace Matrix.Components
{
    public class StatusMessageComponent : ViewComponent
    {
        private readonly ApplicationDbContext _db;

        public StatusMessageComponent(ApplicationDbContext db)
        {
            _db = db;
        }

        public IViewComponentResult Invoke(string status)
        {
            return View(status);
        }
    }
}