﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Matrix.Data;
using Matrix.Models;

namespace Matrix.Components
{
    public class NotificationComponentcs : ViewComponent
    {
        private readonly ApplicationDbContext _db;
        public NotificationComponentcs(ApplicationDbContext db)
        {
            _db = db;
        }

        public IViewComponentResult Invoke()
        {
            var notifications = new Notifications();
             notifications.ChatsNotifications  = new List<ChatsNotifications>() {
                new ChatsNotifications(){
                    MessageId = Guid.NewGuid(),
                    WriterId = Guid.NewGuid() ,
                    Name = "Yaser" , PostMessage = "hi all, please try your best to deliver all tasks on the time ." ,
                    TimeSpan = "1 houre ago" ,
                    WriterphotoLink = "~/images/user-profile.png"
                },
                new ChatsNotifications(){
                    MessageId = Guid.NewGuid(),
                    WriterId = Guid.NewGuid() ,
                    Name = "Rashad" , PostMessage = "Next week we will have meeting in this regard ." ,
                    TimeSpan = "25 min ago" ,
                    WriterphotoLink = "~/images/user-profile.png"
                },
                new ChatsNotifications(){
                    MessageId = Guid.NewGuid(),
                    WriterId = Guid.NewGuid() ,
                    Name = "Ali" , PostMessage = "I will be in leave next week , can you postpond to another date." ,
                    TimeSpan = "30 min ago" ,
                    WriterphotoLink = "~/images/user-profile.png"
                },
                new ChatsNotifications(){
                    MessageId = Guid.NewGuid(),
                    WriterId = Guid.NewGuid() ,
                    Name = "Maitha" , PostMessage = "i have an issue finisdhing my take , the other sections are not collaborating ." ,
                    TimeSpan = "45 min ago" ,
                    WriterphotoLink = "~/images/user-profile.png"
                },
            };

            return View(notifications);
        }
    }
}