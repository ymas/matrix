using System;
using System.Linq;
using System.Threading.Tasks;
using Matrix.Data.DomainClasses;
using Microsoft.AspNetCore.Identity;

namespace Matrix
{
    public class Seeder
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<Role> _roleManager;

        public Seeder(UserManager<ApplicationUser> userManager, RoleManager<Role> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public async Task SeedAll()
        {
            await SeedRoles();
            await SeedAdmin();
        }
        
        public async Task SeedRoles()
        {
            var names = new[] {"Admin", "User"};
            var descriptions = new[] {"System admin", "User"};
            var roles = names.Zip(descriptions, (name, description) => new Role
            {
                Name = name,
                Description = description
            });
            
            foreach (var role in roles)
            {
                await _roleManager.CreateAsync(role);
            }
        }

        public async Task SeedAdmin()
        {
            var admin = new ApplicationUser
            {
                UserName = "admin",
                NormalizedUserName = "ADMIN",
                FullName = "System admin",
                NameAr = "مدير النظام",
                Email = "admin@hotmail.com",
                NormalizedEmail = ("admin@hotmail.com").ToUpper(),
                RegisterDate = DateTime.Now,
                EmailConfirmed = true, Id = Guid.NewGuid(),
                Gender = "male"
                
            };
            if(await _userManager.FindByNameAsync("ADMIN") != null) return;
            await _userManager.CreateAsync(admin, "!QAZxsw2");
            await _userManager.AddToRoleAsync(admin, "Admin");
        }

        
    }
}