﻿
using Matrix.Resources;

namespace System.ComponentModel.DataAnnotations
{
    public class MyRangeAttribute : RangeAttribute
    {
        private readonly double _min; 
        private readonly double _max; 
        public MyRangeAttribute(double min , double max) : base(min , max)
        {
            _min = min;
            _max = max;
        }
        
        public override string FormatErrorMessage(string name)
        {
            ErrorMessage = string.Format(_Translate.RangeAttribute.ToString() , name , _min , _max );
            return base.FormatErrorMessage(name);
        }

        public override bool IsValid(object value)
        {
            if (value == null) return false;
            double.TryParse(value.ToString(), out double xx);
            return xx >= _min && xx <= _max;
        }
        
    }
}