using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace Matrix.Controllers
{
    public class TestController : Controller
    {
        public IActionResult Generate()
        {
            var types = new string[]
            {
                "القتل العمد",
                "الاعتداء البليغ",
                "الاغتصاب",
                "السرقة (السلب+ النهب)",
                "السرقة",
                "الخطف",
                "السرقة / للمركبات",
                "السرقة / سطو",
                "المخدرات",
                "الاتجار بالبشر",
            };


            // 25.294476, 55.333182
            // 25.163002, 55.480781
            // 24.836007, 55.070217
            // 24.929889, 54.960508


            // const double latStart = 24.909598;
            // const double lngStart = 54.896484;
            // const double latEnd = 25.237516;
            // const double lngEnd = 55.563034;

            // const double theta = -45;
            // const double latStart = 24.836007;
            // const double lngStart = 55.070217;
            // const double latEnd = 25.294476;
            // const double lngEnd = 55.333182;

            double[][] pp =
            {
                new[] {25.130910, 55.309759, 25.275324, 55.448112},
                new[] {25.112131, 55.259734, 25.226527, 55.415967},
                new[] { 24.853673, 55.027142, 24.977050, 55.085255},
                new[] { 24.987226,55.116293 , 25.055444, 55.214690},
            };

            const int numberOfPoints = 50;


            var latPoints = Enumerable.Range(0, (int) ((float) numberOfPoints / pp.Length))
                .SelectMany(x => pp.Select(bounds => GetRandomNumberInRange(bounds[0], bounds[2])));
            var lngPoints = Enumerable.Range(0, (int) ((float) numberOfPoints / pp.Length))
                .SelectMany(x => pp.Select(bounds => GetRandomNumberInRange(bounds[1], bounds[3])));

            var positions = latPoints.Zip(lngPoints, (lat, lng) => new Tuple<double, double>(lat, lng));

                const int minPointsPerPosition = 1;
                const int maxPointsPerPosition = 100;

            var points = positions.Select(position => new
            {
                Lat = position.Item1,
                Lng = position.Item2,
                Type = types[new Random().Next(types.Length)],
                Z = new Random().Next(minPointsPerPosition, maxPointsPerPosition)
            });

            return Ok(new
            {
                points,
                types
            });
        }

        public IActionResult MapsOfCriminalMethods()
        {
            var types = new string[]
            {
               "خلع قفل الباب",
               "الاحتيال للاستيلاء على مال الغير",
               "المزاحمة",
               "المغافلة",
               "الاحتيال الإلكتروني",
               "قص الجيب بشفرة حادة",
               "مضاعفة الأموال",
               "انتحال صفة رجال الأمن",
               "تحسس العملة",
            };


            // 25.294476, 55.333182
            // 25.163002, 55.480781
            // 24.836007, 55.070217
            // 24.929889, 54.960508


            // const double latStart = 24.909598;
            // const double lngStart = 54.896484;
            // const double latEnd = 25.237516;
            // const double lngEnd = 55.563034;

            // const double theta = -45;
            // const double latStart = 24.836007;
            // const double lngStart = 55.070217;
            // const double latEnd = 25.294476;
            // const double lngEnd = 55.333182;

            double[][] pp =
            {
                new[] {25.135046,  55.222247, 25.170474,55.412448},
                new[] {25.112131, 55.259734, 25.226527, 55.415967},
                new[] { 24.853673, 55.027142, 24.977050, 55.085255},
                new[] { 24.987226,55.116293 , 25.055444, 55.214690},
            };

            const int numberOfPoints = 50;


            var latPoints = Enumerable.Range(0, (int) ((float) numberOfPoints / pp.Length))
                .SelectMany(x => pp.Select(bounds => GetRandomNumberInRange(bounds[0], bounds[2])));
            var lngPoints = Enumerable.Range(0, (int) ((float) numberOfPoints / pp.Length))
                .SelectMany(x => pp.Select(bounds => GetRandomNumberInRange(bounds[1], bounds[3])));

            var positions = latPoints.Zip(lngPoints, (lat, lng) => new Tuple<double, double>(lat, lng));

                const int minPointsPerPosition = 1;
                const int maxPointsPerPosition = 100;

            var points = positions.Select(position => new
            {
                Lat = position.Item1,
                Lng = position.Item2,
                Type = types[new Random().Next(types.Length)],
                Z = new Random().Next(minPointsPerPosition, maxPointsPerPosition)
            });

            return Ok(new
            {
                points,
                types
            });
        }

        private static double GetRandomNumberInRange(double minNumber, double maxNumber)
        {
            return new Random().NextDouble() * (maxNumber - minNumber) + minNumber;
        }
    }
}