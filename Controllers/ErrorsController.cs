using Matrix.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Ymas.YmasServices;

namespace Matrix.Controllers
{
    public class ErrorsController : Controller
    {
        private readonly IEmailSender _email;
        private readonly IConfiguration _configuration;

        public ErrorsController(IEmailSender email ,IConfiguration configuration)
        {
            _email = email;
            _configuration = configuration;
        }

        // GET
        [Route("Error/{statusCode}")]
        [AllowAnonymous]
        public IActionResult HandleErrorCode(int statusCode)
        {
            var statusCodeData = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();

            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Sorry the page you requested could not be found";
                    ViewBag.RouteOfException = statusCodeData.OriginalPath;
                    break;
                case 403:
                    ViewBag.ErrorMessage = "Sorry you might hasn't the suffusion access to this content!";
                    ViewBag.RouteOfException = statusCodeData.OriginalPath;
                    break;
                case 500:
                    ViewBag.ErrorMessage = "Sorry something went wrong on the server";
                    ViewBag.RouteOfException = statusCodeData.OriginalPath;
                    _email.SendEmailAsync(new EmailSenderModel()
                    {
                        Subject = "Error 500" , ToEmails = new []{"yaser.ahmed@dcas.gov.ae"} , Body = TempData["ErrorException"].ToString()
                    });
                    break;
            }

            return View();
        }
    }
}