﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Matrix.Data;
using Matrix.Data.DomainClasses;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Matrix.Controllers
{
    [Roles("Admin")]
    public class SeedController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IHostingEnvironment _env;
        private readonly ApplicationDbContext _context;
        private static readonly StringBuilder Log = new StringBuilder();
        private readonly string _defaultAdminEmail;

        public SeedController(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration,
            RoleManager<Role> roleManager,
            IHostingEnvironment env)

        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
            _env = env;
            _defaultAdminEmail = configuration["DefaultAdminEmail"];
        }

        public async Task<IActionResult> Seed()
        {
            // create roles 
            var adminUser = $"admin@{_defaultAdminEmail}";
            var adminPassword = "!QAZxsw2";

            if (_env.IsProduction()) return Content("please shift to developing env to continue seeding.");


            // add Admin role
            if (await _roleManager.FindByNameAsync("Admin") == null)
            {
                await _roleManager.CreateAsync(new Role()
                {
                    Id = Guid.NewGuid(),
                    Name = "Admin",
                    Description = "System Admin"
                });
            }


            // add user role
            if (await _roleManager.FindByNameAsync("User") == null)
            {
                await _roleManager.CreateAsync(new Role()
                {
                    Id = Guid.NewGuid(),
                    Name = "User",
                    Description = "System user"
                });
            }


            // add user role
            if (await _roleManager.FindByNameAsync("Matrix") == null)
            {
                await _roleManager.CreateAsync(new Role()
                {
                    Id = Guid.NewGuid(),
                    Name = "Matrix",
                    Description = "Matrix admission"
                });
            }

            // create the default user 
            var admin = new ApplicationUser()
            {
                Id = Guid.NewGuid(),
                UserName = adminUser,
                Email = adminUser,
                FullName = "System admin",
                RegisterDate = DateTime.Now
            };

            if (!_context.Users.Any(a => a.Email == adminUser))
            {
                var save = await _userManager.CreateAsync(admin, adminPassword);
                if (save.Succeeded)
                {
                    Log.AppendLine($"User Admin inserted successfully");
                    // AddPhoneNumberViewModel user claims 
                    await _userManager.AddClaimAsync(admin, new Claim("DisplayName", admin.FullName));
                    
                    var role = await _roleManager.FindByNameAsync("Admin");
                    if (role == null)
                    {
                        var addRole = await _roleManager.CreateAsync(new Role()
                        {
                            Id = Guid.NewGuid(),
                            Name = "Admin",
                            Description = "System Admin"
                        });
                        if (addRole.Succeeded)
                        {
                            role = await _roleManager.FindByNameAsync("Admin");
                        }
                    }

                    if (role != null && !_context.UserRoles.Any(a => a.UserId == admin.Id && a.RoleId == role.Id))
                    {
                        var saveRole = await _userManager.AddToRoleAsync(admin, "Admin");
                        if (saveRole.Succeeded)
                        {
                            Log.AppendLine($"user Admin added to role Admin successfully");
                        }
                        else
                        {
                            Log.AppendLine($"Failed to add user Admin to role Admin !");

                        }

                    }
                }
                else
                {
                    Log.AppendLine($"Failed to insert user 'Admin' !");
                }
            }
            else
            {
                Log.AppendLine("user  'Admin' already exists");
            }

            return Content(Log.ToString());
        }



        

    }


}
  
