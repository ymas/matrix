﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Matrix.Data;
using Matrix.Models.ModelViews;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Matrix.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _db;
        private string[] CriminalMethods { get; set; }
        private string[] CrimeTools { get; set; }
        private string[] CrimeRemains { get; set; }

       
        public HomeController(ApplicationDbContext db)
        {
            _db = db;
            CriminalMethods = new[]
            {
                "خلع قفل الباب",
                "الاحتيال للاستيلاء على مال الغير",
                "المزاحمة",
                "المغافلة",
                "الاحتيال الإلكتروني",
                "قص الجيب بشفرة حادة",
                "مضاعفة الأموال",
                "انتحال صفة رجال الأمن",
                "تحسس العملة",
                "تسلق السور",
            };
            CrimeTools = new string[]
            {
                "مفك",
                "سكين",
                "مسدس",
                "عتلة",
                "مضرب بيسبول",
                "كماشة",
                "حبل",
                "بطاقة إئتمانية",
                "مركبة",
            };
            
            CrimeRemains = new string[]
            {
                "عود ثقاب",
                "فلتر سجائر",
                "بصمة",
                "دماء",
                "مفك",
                "بصمة حذاء",
            };
        }


        

       
        public ActionResult Index()
        {

            var model = new HomeModelView()
            {
                
               
            };

            return View(model);
        }
        
        public ActionResult Maps()
        {

            return View();
        }
        
        public class CriminalCard
        {
            public string Name { get; set; }
            public string Location { get; set; }
            public string Status { get; set; }
            public string Photo { get; set; }
            public string Level { get; set; }
            public string Other { get; set; }
        }
        
        public ActionResult UnknownCases()
        {
            var model = new List<CriminalCard>()
            {
                new CriminalCard()
                {
                    Name = "محمد عظيم خان - باكتاني", 
                    Level = "متمرس - ١٢ سابقة", 
                    Location = "السطوة", 
                    Photo = "1", 
                    Status = "داخل الدولة"
                },
                new CriminalCard()
                {
                    Name = "محمد صنقور بلال -  الامارات", 
                    Level = "مقلد - ٤ سوابق", 
                    Location = "القصيص", 
                    Photo = "4", 
                    Status = "متواجد بالدولة"
                },
                new CriminalCard()
                {
                    Name = "ديمتري جاش يروند - روسي", 
                    Level = "خطر - ١٥ سابقة", 
                    Location = "الجافلية منزل رقم ٤٥", 
                    Photo = "2", 
                    Status = "مسجون",
                    Other = "عضو في عصابة"
                },
                new CriminalCard()
                {
                    Name = "عبيد راشد محمد", 
                    Level = "مبتدء - سابقة", 
                    Location = "النجمة", 
                    Photo = "3", 
                    Status = "داخل الدوله",
                    Other = "مفرج عنه حديثاُ"
                },
               
                
            };
            ViewBag.MethodsMenu = 
                new SelectList(this.CriminalMethods.Select(a => new
            {
                id = a, text = a
            }), "id","text");
            return View(model);
        }
        
        [HttpPost]
        public ActionResult UnknownCases(CriminalCard card)
        {
            var model = new List<CriminalCard>()
            {
                new CriminalCard()
                {
                    Name = "محمد عظيم خان - باكتاني", 
                    Level = "متمرس - ١٢ سابقة", 
                    Location = "السطوة", 
                    Photo = "1", 
                    Status = "داخل الدولة"
                },
                new CriminalCard()
                {
                    Name = "محمد صنقور بلال -  الامارات", 
                    Level = "مقلد - ٤ سوابق", 
                    Location = "القصيص", 
                    Photo = "4", 
                    Status = "متواجد بالدولة"
                },
                new CriminalCard()
                {
                    Name = "ديمتري جاش يروند - روسي", 
                    Level = "خطر - ١٥ سابقة", 
                    Location = "الجافلية منزل رقم ٤٥", 
                    Photo = "2", 
                    Status = "مسجون",
                    Other = "عضو في عصابة"
                },
                new CriminalCard()
                {
                    Name = "عبيد راشد محمد", 
                    Level = "مبتدء - سابقة", 
                    Location = "النجمة", 
                    Photo = "3", 
                    Status = "داخل الدوله",
                    Other = "مفرج عنه حديثاُ"
                },
               
                
            };
            ViewBag.MethodsMenu = 
                new SelectList(this.CriminalMethods.Select(a => new
                {
                    id = a, text = a
                }), "id","text");
            return View(model.Skip(new Random().Next(1,4)));
        }

        public ActionResult Methods()
        {

            return View();
        }


        
        [AllowAnonymous]
        public IActionResult Error(int? statusCode)
        {
            ViewData["Message"] = "";
            ViewData["Title"] = "Status code " + statusCode;
            switch (statusCode)
            {
                case 404 : ViewData["Message"] = "الصفحة التي تبحث عنها غير متوفره حالياً";
                    break;
                case 400 : ViewData["Message"] = "الصفحة التي تبحث عنها غير متوفره حالياً";
                    break;
                case StatusCodes.Status403Forbidden : ViewData["Message"] = "ليس لديك الصلاحية لعرض محتويات هذه الصفحة!";
                    break;
                case StatusCodes.Status500InternalServerError : ViewData["Message"] = "نعتذر .. النظام غير قادر على عرض محتويات هذه الصفحة! الرجاء التواصل مع مسؤول الدعم الفني لتقديم المساعدة";
                    break;
            }
            return View();
        }

        public ActionResult Contact(Guid id)
        {
            return Content("rate :");
        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult ChangeCulture(string culture)
        {
            var newCulture = string.IsNullOrEmpty(culture) ? "ar-AE" : culture;
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(newCulture)),
                new CookieOptions {Expires = DateTimeOffset.UtcNow.AddYears(1), HttpOnly = true , Secure = Request.IsHttps}
            );
            var returnUrl = Request.Headers["Referer"];
            if (string.IsNullOrEmpty(Request.Headers["Referer"])) return RedirectToAction("Index", "Home");
            var user = _db.Users.Find(this.GetUserId());
            if (user != null && culture != null)
            {
                user.IsEnglishLanguage = culture.Equals("en-US");
                _db.Entry(user).State = EntityState.Modified;
                _db.SaveChanges();
            }
            return Redirect(returnUrl);
        }


        [AllowAnonymous]
        public IActionResult NotSupportedBrowser()
        {
            return View();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}