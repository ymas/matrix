﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Matrix.Data;
using Matrix.Data.DomainClasses;
using Matrix.Models;
using Matrix.Models.AccountViewModels;
using Matrix.Resources;
using Matrix.Static;

namespace Matrix.Controllers
{

    [Authorize(Roles = "Admin")]
    public class UsersAdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _db;
        private readonly RoleManager<Role> _roleManager;
        
        public UsersAdminController(
            UserManager<ApplicationUser> userManager,
            ApplicationDbContext db,
            RoleManager<Role> roleManager)
        {
            _userManager = userManager;
            _db = db;
            _roleManager = roleManager;
        }

        public async Task<string> UserRolesAsString(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            var userRoles = await _userManager.GetRolesAsync(user);
            var r = userRoles.ToArray().Length > 0 ? string.Join(" - ", userRoles) : string.Empty;
            return r;
        }


        public ActionResult Index()
        {
            var users = _db.Users.OrderBy(a => a.RegisterDate).ToList().Select(u => new UsersList()
            {
                EmployeeId = u.EmployeeNo,
                UserName = u.UserName,
                FullName = u.FullName,
                RegisterDate = u.RegisterDate.ToString(CultureInfo.CurrentCulture),
                Roles = UserRolesAsString(u.Id).Result,
                UserId = u.Id.ToString()
            }).ToList();
            foreach (var item in users)
            {
                item.Roles = item.Roles;
            }
            return View(users);
        }


        //
        // GET: /Users/Details/5
        public async Task<ActionResult> Details(Guid id)
        {
           
            var user = await _userManager.FindByIdAsync(id.ToString());

            ViewBag.RoleNames = await _userManager.GetRolesAsync(user);

            return View(user);
        }

        //
        // GET: /Users/Create
        
        public async Task<ActionResult> Create()
        {
            //Get the list of Roles
            var roles = await _roleManager.Roles.ToListAsync();
            List<RoleCheckBoxViewModel> rolesCheckBoxes = new List<RoleCheckBoxViewModel>();
            foreach (var item in roles)
            {
                rolesCheckBoxes.Add(new RoleCheckBoxViewModel()
                { 
                ID = item.Id,
                Name = item.Name ,
                Desc = item.Description
                });
            }
            ViewBag.RoleId = new SelectList(rolesCheckBoxes, "Name", "Desc");

            return View();
        }

        //
        // POST: /Users/Create
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            //Get the list of Roles
            var roles = await _roleManager.Roles.ToListAsync();
            List<RoleCheckBoxViewModel> rolesCheckBoxes = new List<RoleCheckBoxViewModel>();
            foreach (var item in roles)
            {
                rolesCheckBoxes.Add(new RoleCheckBoxViewModel()
                {
                    ID = item.Id,
                    Name = item.Name,
                    Desc = item.Description
                });
            }
            ViewBag.RoleId = new SelectList(rolesCheckBoxes, "Name", "Desc");


            if (ModelState.IsValid)
            {
               
                var user = new ApplicationUser { UserName = userViewModel.UserName, Email = userViewModel.UserName };
                var adminresult = await _userManager.CreateAsync(user, userViewModel.Password);

                //Add User to the selected Roles 
                if (adminresult.Succeeded)
                {


                    if (selectedRoles != null)
                    {
                        var result = await _userManager.AddToRolesAsync(user, selectedRoles);
                        if (!result.Succeeded)
                        {
                            AddErrors(result);
                            return RedirectToAction("Edit", user.Id);
                        }
                    }
                }
                else
                {
                    AddErrors(adminresult);
                    return View(userViewModel);

                }
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(_roleManager.Roles, "Name", "Name");
            //ViewBag.UserDepID = list;
            return View();
        }

        //edit user password
        public async Task<ActionResult> EditPassword(Guid id)
        {
           
            var user = await _userManager.FindByIdAsync(id.ToString());
            return View(new EditPasswordModelView() { UserID = user.Id });
        }

        [HttpPost]
        public async Task<ActionResult> EditPassword(EditPasswordModelView edit)
        {
            var user = await _userManager.FindByIdAsync(edit.UserID.ToString());
            if (user == null)
            {
                return MyHttp.NotFound();
            }
            if (!string.IsNullOrWhiteSpace(edit.Password))
            {
                await _userManager.GeneratePasswordResetTokenAsync(user);
            }
            return RedirectToAction("Index");
        }
        // GET: /Users/Edit/1
        public async Task<ActionResult> Edit(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                return MyHttp.NotFound();
            }

            var userRoles = await _userManager.GetRolesAsync(user);

            var roles = await _roleManager.Roles.ToListAsync();
            List<RoleCheckBoxViewModel> rolesCheckBoxes = new List<RoleCheckBoxViewModel>();
            foreach (var item in roles)
            {
                rolesCheckBoxes.Add(new RoleCheckBoxViewModel()
                {
                    ID = item.Id,
                    Name = item.Name,
                    Desc = item.Description
                });
            }


            var viewObj = new EditUserViewModel
            {
                Id = user.Id,
                Email = user.Email,
                RolesList = rolesCheckBoxes.Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name), Text = x.Desc, Value = x.Name
                })
            };




            return View(viewObj);
        }

        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit( EditUserViewModel editUser, params string[] selectedRole)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByIdAsync(editUser.Id.ToString());
                if (user == null)
                {
                    return MyHttp.NotFound();
                }

                user.UserName = editUser.EmployeeId.ToString();
                user.Email = editUser.EmployeeId.ToString();

                var userRoles = await _userManager.GetRolesAsync(user);

                var roles = await _roleManager.Roles.ToListAsync();
                List<RoleCheckBoxViewModel> rolesCheckBoxes = new List<RoleCheckBoxViewModel>();
                foreach (var item in roles)
                {
                    rolesCheckBoxes.Add(new RoleCheckBoxViewModel()
                    {
                        ID = item.Id,
                        Name = item.Name,
                        Desc = item.Description
                    });
                }
                selectedRole = selectedRole ?? new string[] { };

                var result = await _userManager.AddToRolesAsync(user, selectedRole.Except(userRoles).ToArray());

                if (!result.Succeeded)
                {
                    AddErrors(result);
                    return View();
                }
                result = await _userManager.RemoveFromRolesAsync(user, userRoles.Except(selectedRole).ToArray());

                if (!result.Succeeded)
                {
                    AddErrors(result);
                    return View();
                }

                return RedirectToAction("Index");
            }
            ModelState.AddModelError("", "Something failed.");
            return View();
        }

        //
        // GET: /Users/Delete/5
        public async Task<ActionResult> Delete(Guid id)
        {
            
            var user = await _userManager.FindByIdAsync(id.ToString());
            if (user == null)
            {
                return MyHttp.NotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            //int del = 0;
            if (ModelState.IsValid)
            {
                
                var user = await _db.Users.FindAsync(id);
                if (user == null)
                {
                    return MyHttp.NotFound();
                }

                var result = await _userManager.DeleteAsync(user);

                    if (!result.Succeeded)
                    {
                        AddErrors(result);
                        return View();
                    }

                    TempData["Message"] = _Translate.DeletedSuccessfully;
                return RedirectToAction("Index","Account");
            }
            return View();
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        #endregion

        public IActionResult Roles()
        {
            var roles = _db.Roles;
            return View(roles.ToList());        }
    }
}
