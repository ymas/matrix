

using System.Collections.Generic;
using Matrix.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Matrix.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AreaController : Controller
    {
        private readonly ApplicationDbContext _db;

        public AreaController(ApplicationDbContext db)
        {
            this._db = db;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string name)
        {
            var item = new Area()
            {
                Name = name,
                IsDeleted = false
            };
            _db.Areas.Add(item);
            _db.SaveChanges();

            return Json(new
            {
                Result = 1,
                Code = 0x00,
                Message = "Success",
                Extra = new
                {
                    Text = name,
                    Value = item.Id
                }
            });
        }

        public ActionResult AddNew()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddNew(Area area)
        {
            if (ModelState.IsValid)
            {
                _db.Areas.Add(area);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(area);
        }

        public ActionResult Edit(int id)
        {
            return View("AddNew", _db.Areas.Find(id));
        }

        [HttpPost]
        public ActionResult Edit(Area area)
        {
            if (ModelState.IsValid)
            {
                _db.Entry(area).State = EntityState.Modified;
                _db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("AddNew", area);
        }

        [HttpPost]
        public ActionResult Remove(int id)
        {
            _db.Areas.Find(id).IsDeleted = true;
            _db.SaveChanges();
            return Json(new {Result = 1, Extra = "Deleted"});
        }

        public ActionResult GetMore(int start, int length, int draw, Dictionary<string, string> search)
        {
            var items = _db.Areas.Where(x => x.IsDeleted == false);
            var totalCount = items.Count();
            var searchTerm = search["value"];
            items = items.Where(x => x.Name.Contains(searchTerm));
            var totalFiltered = items.Count();
            var finalItems = items.OrderBy(x => x.Id).Skip(start).Take(length > 0 ? length : 99999999).ToList().Select(
                    x => new List<string>
                    {
                        x.Id.ToString(),
                        x.Name,
                        "<a href='/matrix/Area/Edit/" + x.Id + "'>تعديل</a> | " +
                        "<a id='" + x.Id + "' class='remove'>حذف</a>"
                    })
                .ToList();
            return Json(new
            {
                data = finalItems,
                recordsTotal = totalCount,
                recordsFiltered = totalFiltered,
                draw = draw
            }, JsonRequestBehavior.AllowGet);
        }
    }
}