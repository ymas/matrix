﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Omu.ValueInjecter;
using Matrix.Extensions;
using Matrix.Models.ModelViews;
using Matrix.Data;
using Matrix.Data.DomainClasses;
using Matrix.Dtos.Users;
using Matrix.Interfaces;
using Matrix.Models;
using Matrix.Models.AccountViewModels;
using Matrix.Static;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Ymas.Extensions;

namespace Matrix.Controllers
{
    [Route("[controller]/[action]")]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger<AccountController> _logger;
        private readonly ApplicationDbContext _db;
        private readonly RoleManager<Role> _roleManager;
        private readonly string _entityDomain;
        private readonly string _defaultAdminEmail;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ApplicationDbContext db,
            RoleManager<Role> roleManager,
            ILogger<AccountController> logger,
            IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _db = db;
            _roleManager = roleManager;
            _entityDomain = configuration["Email:EntityDomain"];
            _defaultAdminEmail = configuration["DefaultAdminEmail"];
        }

        private List<RolesCheckbox> InitialCheckedRoles(EditRegisterViewModel model)
        {
            var r = (from x in _db.Roles
                select new RolesCheckbox()
                {
                    Checked = (model.Roles.Contains(x.Name) ? "checked" : ""),
                    Name = x.Name,
                    RoleId = x.Id,
                    RoleName = x.Description
                }).ToList();
            return r;
        }


        [AllowAnonymous]
        public IActionResult PartialAccessDenied()
        {
            return PartialView("_AccessDenied");
        }

        
        [Roles("Admin")]
        public ActionResult Index()
        {
            var users = _db.Users.Select(UsersDto.Mapper(this.IsEn()))
                .OrderBy(a => a.RegisterDate)
                .ToList();

            var roles = _db.Roles.ToList();

            foreach (var item in users)
            {
                var uRolesIds = _db.UserRoles.Where(a => a.UserId == item.Id).Select(a => a.RoleId).ToArray();
                item.Roles = string.Join(" , ",
                    roles.Where(r => uRolesIds.Contains(r.Id)).Select(r => r.Name).ToArray());
            }

            return View(users);
        }

        

        // GET: /Account/Register
        [Roles("Admin")]
        public ActionResult Register()
        {
            var model = new RegisterViewModel()
            {
                Roles = new []{"User"}
            };
            PrepareUser(model);
            return View();
        }


        //
        // POST: /Account/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Roles("Admin")]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userName = (Regex.Replace(model.Email, "@.*$", "", RegexOptions.IgnoreCase));
                var user = new ApplicationUser
                {
                    Id = Guid.NewGuid(),
                    Email = model.Email,
                    NormalizedEmail = model.Email.ToUpper(),
                    UserName = userName.ToLower(),
                    NormalizedUserName = userName.ToUpper(),
                    RegisterDate = DateTime.Now,
                    FullName = model.FullName,
                    EmployeeNo = model.EmployeeNo,
                    NameAr = model.NameAr,
                    Photo = model.Photo != null ?  MyStatics.ResizeImageWithMaxDimension(model.Photo ,300) : null,
                    Gender = model.Gender.ToString()
                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    foreach (var item in model.Roles)
                    {
                        var isRole = await _roleManager.RoleExistsAsync(item);
                        if (isRole)
                        {
                            await _userManager.AddToRoleAsync(user, item);
                        }

                        // send notification to user email 
                        var msg = new StringBuilder();
                        msg.AppendLine($"Dear {user.UserName.Split(' ').FirstOrDefault()}");
                        msg.AppendLine(
                            $"Hope you are doing well, this email is about to inform you that new account has been created for you in the 4G Model application.");
                        msg.AppendLine("<br/>");
                        msg.AppendLine($"To access the application please follow the instructions");
                        msg.AppendLine(
                            $"<ol><li> go to login page <a target=\"_blank\" href=\"{Request.Scheme}://{Request.Host}/Account/login\"\">login</a></li>");
                        msg.AppendLine($"<li> your username is : {user.UserName} </li>");
                        msg.AppendLine($"<li> your password is : {model.Password} </li>");
                        msg.AppendLine(
                            $"<li> please don't copy and paste from this email, it may copy empty space along with password, instead write the login information manually. </li>");
                        msg.AppendLine("</ol>");
                        msg.AppendLine("<br/>");
                        msg.AppendLine("<br/>");
                        msg.AppendLine("<br/>");
                        msg.AppendLine("<br/>");
                        msg.AppendLine("<br/>");
                        msg.AppendLine("<br/>");

                        msg.AppendLine("Best regards");
                        msg.AppendLine("<br/>");
                        msg.AppendLine("4G Model Application");
                        var mailer = new EmailSenderModel()
                        {
                            ToEmails = new[] {model.Email},
                            Subject = "Your 4G Model Account",
                            Body = msg.ToString()
                        };
                        try
                        {
                            await _emailSender.SendEmailAsync(mailer);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                            return MyStatics.GetResponse(0, null, "Error: error return with exception!");
                        }
                    }

                    return RedirectToAction("Index");
                }

                AddErrors(result);
            }

            // If we got this far, something failed, redisplay form
            PrepareUser(model);
            return View(model);
        }

        private void PrepareUser(RegisterViewModel model)
        {
            ViewBag.Roles = _db.Roles.ToList();
            ViewBag.SelectedRoles = model?.Roles.ToList();
        }

        private void PrepareEditUser(EditRegisterViewModel model)
        {
            ViewBag.Roles = _db.Roles.ToList();
            ViewBag.SelectedRoles = model?.Roles.ToList();
        }


        [HttpGet]
        [Roles("Admin")]
        public async Task<ActionResult> EditRegister(Guid id)
        {
            var u = await _userManager.FindByIdAsync(id.ToString());
            if (u == null)
            {
                return StatusCode((int) HttpStatusCode.NotFound);
            }

            var userRoles = await _userManager.GetRolesAsync(u);
            var model = new EditRegisterViewModel()
            {
                Id = id,
                FullName = u.FullName,
                NameAr = u.NameAr,
                UserName = u.UserName,
                Email = u.Email,
                EmployeeNo = u.EmployeeNo,
                Roles = userRoles.ToArray(),
                Gender = Enum.Parse<Gender>(u.Gender.FirstCharToUpper())
            };
            model.SelectedRoles = InitialCheckedRoles(model);
            PrepareEditUser(model);
            return View(model);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Roles("Admin")]
        public async Task<ActionResult> EditRegister(EditRegisterViewModel model)
        {
            model.SelectedRoles = InitialCheckedRoles(model);
            PrepareEditUser(model);
            var totalUsers = _db.Users.Count(a => a.Email == model.Email.ToLower());
            if (totalUsers > 1)
            {
                ModelState.AddModelError(string.Empty, "The email is already in use!");
                return View(model);
            }

            if (!ModelState.IsValid) return View(model);
            var user = await _userManager.FindByIdAsync(model.Id.ToString());
            
            user.InjectFrom(model);
            if (model.Photo != null)
            {
                user.Photo = MyStatics.ResizeImageWithMaxDimension(model.Photo ,300);
            }
            user.Email = model.Email.ToLower();
            user.NormalizedEmail = model.Email.ToUpper();
            user.Gender = model.Gender.ToString();

            _db.Entry(user).State = EntityState.Modified;
            var result = _db.SaveChanges();
            if (result != 1)
            {
                ModelState.AddModelError("Error", errorMessage: ModelState.GetErrorsAsList());
                return View(model);
            }

            if (!string.IsNullOrEmpty(model.Password))
            {
                var token = await _userManager.GeneratePasswordResetTokenAsync(user);
                var res = await _userManager.ResetPasswordAsync(user, token, model.Password);
                if (!res.Succeeded)
                {
                    ModelState.AddModelError("Password Error", String.Empty);
                    AddErrors(res);
                    return View(model);
                }
                else
                {
                    // send notification to user email 
                    var msg = new StringBuilder();
                    msg.AppendLine($"Dear {user.UserName.Split(' ').FirstOrDefault()}");
                    msg.AppendLine("<br/>");
                    msg.AppendLine($"Hope you are doing well, your password has been changed by system admin.");
                    msg.AppendLine($"To access the application please follow the instructions");
                    msg.AppendLine(
                        $"<ol><li> go to login page <a target=\"_blank\" href=\"{Request.Scheme}://{Request.Host}/Account/login\"\">login</a></li>");
                    msg.AppendLine($"<li> your username is : {user.UserName} </li>");
                    msg.AppendLine($"<li> your password is : {model.Password} </li>");
                    msg.AppendLine(
                        $"<li> please don't copy and past from this email, it may copy empty space along with password, instead write the login information manually. </li>");
                    msg.AppendLine("</ol>");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("<br/>");

                    msg.AppendLine("Best regards");
                    msg.AppendLine("<br/>");
                    msg.AppendLine("4G Model Application");
                    var mailer = new EmailSenderModel()
                    {
                        ToEmails = new[] {model.Email},
                        Subject = "New password 4G Model Account",
                        Body = msg.ToString()
                    };
                    try
                    {
                        await _emailSender.SendEmailAsync(mailer);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        return MyStatics.GetResponse(0, null, "Error: error return with exception!");
                    }
                }
            }


            if (!model.Roles.Any()) return RedirectToAction("Index", "Account");
            var userRoles = _userManager.GetRolesAsync(user);
            var remove = await _userManager.RemoveFromRolesAsync(user, userRoles.Result);
            if (remove.Succeeded)
            {
                var rolesUpper = model.Roles.Select(a => a.ToUpper()).ToArray();
                var rolesUpdateResult = await _userManager.AddToRolesAsync(user, rolesUpper);
                if (rolesUpdateResult.Succeeded) return RedirectToAction("Index");
                ModelState.AddModelError("Roles Error", String.Empty);
                return View(model);
            }
            else
            {
                ModelState.AddModelError("Error", String.Empty);
                return View(model);
            }

            //  return RedirectToAction("Index");
            // If we got this far, something failed, redisplay form
        }


        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            if (Request.Cookies.All(a => a.Key != CookieRequestCultureProvider.DefaultCookieName))
            {
                return RedirectToAction("ChangeCulture", "Home", new {cultur = "ar-AE", returnUrl = "/Account/Login"});
            }

            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe,
                    lockoutOnFailure: false);
                if (result.Succeeded)
                {
                    _logger.LogInformation("User logged in.");
                    var user = await  _userManager.FindByNameAsync(model.Email);
                    if (user != null && user.IsEnglishLanguage)
                    {
                        Response.Cookies.Append(
                            CookieRequestCultureProvider.DefaultCookieName,
                            CookieRequestCultureProvider.MakeCookieValue(new RequestCulture("en-US")),
                            new CookieOptions {Expires = DateTimeOffset.UtcNow.AddYears(1), HttpOnly = true , Secure = Request.IsHttps}
                        );
                    }
                    
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    TempData["InvalidLogin"] = true;
                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        //[ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            _logger.LogInformation("User logged out.");
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }

            var result = await _userManager.ConfirmEmailAsync(user, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPassword()
        {
            return View();
        }


        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                // remove the email extension
                var user = await _userManager.FindByEmailAsync(model.Email.ToUpper());
                if (user == null /*|| !(await _userManager.IsEmailConfirmedAsync(user))*/)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    TempData["Message"] = "Email is incorrect!";
                    return View();
                }

                // For more information on how to enable account confirmation and password reset please
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id.ToString(), code, Request.Scheme);

                if (model.Email.ToLower() == _defaultAdminEmail)
                {
                    model.Email = _defaultAdminEmail;
                }

                if (model.Email.IndexOf('@') < 0)
                {
                    model.Email = model.Email + _entityDomain;
                }

                var msg = new StringBuilder();
                msg.AppendLine("Please reset your password by clicking here: ");
                msg.AppendLine("<a href=\"" + callbackUrl + "\">link</a>");
                msg.AppendLine("Or copy this URL to the browser address :");
                msg.AppendLine(callbackUrl);
                var mailer = new EmailSenderModel()
                {
                    ToEmails = new[] {model.Email},
                    Subject = "Reset Password",
                    Body = msg.ToString()
                };
                try
                {
                    await _emailSender.SendEmailAsync(mailer);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return MyStatics.GetResponse(0, null, "Error: error return with exception!");
                }

                return RedirectToAction("ForgotPasswordConfirmation");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }

            var model = new ResetPasswordViewModel {Code = code};
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }

            var result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction(nameof(ResetPasswordConfirmation));
            }

            AddErrors(result);
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult ResetPasswordConfirmation()
        {
            return View();
        }


        [HttpGet]
        [AllowAnonymous]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpDelete]
        [AllowAnonymous]
        public IActionResult DeleteAccessDenied()
        {
            return View();
        }


        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }

        #endregion

        private async Task<string> UserRolesAsString(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            var userRoles = await _userManager.GetRolesAsync(user);
            var r = userRoles.ToArray().Length > 0 ? string.Join(" - ", userRoles) : string.Empty;
            return r;
        }
    }
}