﻿using System;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Matrix.Data;
using Matrix.Data.DomainClasses;
using Matrix.Resources;
using Matrix.Static;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Omu.ValueInjecter;
using Ymas.Extensions;

namespace Matrix.Controllers
{
 
    [Roles("Admin")]
    public class DepartmentsController : Controller
    {
        private readonly ApplicationDbContext _db;

        public DepartmentsController(ApplicationDbContext db)
        {
            _db = db;
        }
        
        public ActionResult Index(Guid? parentId=null)
        {
            // get all departments recursively 
            var allDep = _db.Departments
                .Where(a => !a.IsDeleted)
                .OrderBy(a => a.ParentId).ThenBy(a => a.NameAr)
                .ToList()
                .SelectRecursive(a=>a.ChildDepartments)
                .Distinct()
                .ToList();
            ViewBag.Title = "";
            if (parentId != null)
            {
                allDep = allDep.Where(a => a.ParentId == parentId).ToList();
            }

            return View(allDep);

        }

        public ActionResult DeletedDepartments()
        {
            var dep = _db.Departments.Where(a => a.IsDeleted).OrderByDescending(a=>a.DeleteDate).ToList();
            return View(dep);
        }
        
        [HttpDelete]
        public ActionResult Restore(Guid? id)
                {
                    if (id == null) return  new StatusCodeResult((int)HttpStatusCode.BadRequest);
                    var item = _db.Departments.Find(id);
                    if(item == null) return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    item.IsDeleted = false;
                    _db.Entry(item).State = EntityState.Modified;
                    var success = _db.SaveChanges();
                     return Json(new{success});
                }
        
        public ActionResult Create()
        {

            PrepareDepartmentLists(null);
            return View();
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Department department)
        {
            if (ModelState.IsValid)
            {
                if (_db.Departments.Any(a => a.NameAr == department.NameAr))
                {
                    TempData["Message"] = department.NameAr+" تم ادخاله سابقا !!";
                    return View(department);            
                }
                department.Id = Guid.NewGuid();
                _db.Departments.Add(department);
                _db.SaveChanges();
                department.NameAr = "";
                department.NameEn = "";
                PrepareDepartmentLists(department);
                TempData["Message"] = _Translate.DataSaevedSuccessfully;
                return View(department);            
            }
            PrepareDepartmentLists(department);
            return View(department);
        }


        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadRequest);
            }
            var dep = _db.Departments.Find(id);
            if(dep == null) return MyHttp.NotFound();
            
            PrepareDepartmentLists(dep);
            return View(dep);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Department depatment)
        {
            if (ModelState.IsValid)
            {
                var dep = _db.Departments.Find(depatment.Id);
                dep.InjectFrom(depatment);
                _db.SaveChanges();
                return RedirectToAction("Index");
            }
            PrepareDepartmentLists(depatment);
            return View(depatment);
        }

        [HttpDelete]
        public ActionResult Delete(Guid? id)
        {
            if(id == null) return new StatusCodeResult((int)HttpStatusCode.BadRequest);
            var item = _db.Departments.Find(id);
            if(item == null) return new StatusCodeResult((int)HttpStatusCode.NotFound);
            _db.Departments.Remove(item);
            var success = _db.SaveChanges();
            return Json(new{success});
        }
        

        
        
        private void PrepareDepartmentLists(Department department)
        {
            ViewBag.Type = Constants.DepartmentType.SelectList(department?.Type);
            var dep = _db.Departments.Where(a =>
                    a.Type == Constants.DepartmentType.Department || a.Type == Constants.DepartmentType.Division)
                .ToList()
                .Select(a => new 
                {
                    Text = a.NameAr, Value = a.Id.ToString(), Group =  a.ParentDepartment?.NameAr ?? a.NameAr
                }).ToList();
            ViewBag.ParentId = new SelectList(dep, "Value" , "Text" , department?.ParentId,"Group");
        }
        
        protected override void Dispose(bool disposing)
                {
                    if (disposing)
                    {
                        _db.Dispose();
                    }
                    base.Dispose(disposing);
                }

    }
}