﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Matrix.Data;

namespace Matrix.Controllers
{
    [Authorize(Roles = "Admin")]
    public class RolesAdminController : Controller
    {
        private readonly ApplicationDbContext _db;

        public RolesAdminController(
            ApplicationDbContext db
        )
        {
            _db = db;
        }


        // GET: /Roles/
        public IActionResult Index()
        {
            var roles = _db.Roles;
            return View(roles.ToList());
        }

    }
}
