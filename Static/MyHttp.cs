﻿using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace Matrix.Static
{
    public static class MyHttp
    {
        public static StatusCodeResult NotFound()
        {
            return new StatusCodeResult((int)HttpStatusCode.NotFound);
        }
        
        public static JsonResult JsonIdNotExists()
        {
            return new JsonResult(MyStatics.GetResponse(0,null,"id is null!!"));
        }
        
        public static StatusCodeResult BadRequest()
        {
            return new StatusCodeResult((int)HttpStatusCode.BadRequest);
        }
    }
}