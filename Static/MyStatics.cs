﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Resources;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Matrix.Resources;
using Matrix.Services;
using Microsoft.AspNetCore.Localization;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using static System.String;

namespace Matrix
{
    public static class MyStatics
    {
        private static readonly ResourceManager ResourceManager = new ResourceManager(typeof(_Translate));
        private static readonly MyHttpContext MyHttp = new MyHttpContext(new HttpContextAccessor());
        public static string DefaultCookieName = ".AspNetCore.Culture";
        public static string PowerUserRoles = "Admin,Matrix";

        public static string GetRefererUrl()
        {
            return MyHttp.GetHttpContext()?.Request.Headers["Referer"].ToString();
        }

        public static string FindResByName(string resName)
        {
            if(IsNullOrEmpty(resName))  return "-"; 
            var cLang =MyHttp.GetHttpContext().Features.Get<IRequestCultureFeature>().RequestCulture.UICulture;
            var res =ResourceManager.GetString(resName, cLang);
            return IsNullOrEmpty(res) ? resName : res;
        }


        
        public static string TransableName(string ar, string en)
        {
            if(string.IsNullOrEmpty(ar) && string.IsNullOrEmpty(en)) return "";
            var trName = ar;
            var cLang = CultureInfo.CurrentUICulture.Name;
            if (cLang == null || cLang != "en-US") return trName;
            return !IsNullOrEmpty(en) ? en : trName;
        }

        
        public static async Task<byte[]> GetBytesFromUrlAsync(string url)
        {
            byte[] imageData = null;

            using (var wc = new HttpClient())
            {
                imageData = await wc.GetByteArrayAsync(url);
            }
            return imageData;
        }

        public static JsonResult DatatableEmpltyJsonRespons(string message)
        {
            var obj = new
            {
                data = new{}, draw = 0, recordsTotal = 0, recordsFiltered = 0 , error = message 
            };
            return new JsonResult(obj);
        }
        
        public static float GetPercent(float x, float of)
        {
            // x/of*100
            if (x.Equals(0) || of.Equals(0)) return 0;
            var res = (float)Math.Round((x/of)*100,1);
            return res;
        }
        
        
        public static double GetPercent(double? x, double? of)
        {
            // x/of*100
            if (x == null || of == null) return 0;
            var res = Math.Round((double)(x/of)*100,1);
            return res;
        }

        public static double GetAvarage(double? x, double? total)
        {
            if (x == null || total == null) return 0;
            if (total.Value.Equals(0)) return 0;
            var res = Math.Round((double)(x/total),1);
            return res;
        }

       
        
        public static JsonResult GetResponse(int _success, object _extras, string _error)
        {
            var obj = new
            {
                success = _success,
                extras = _extras,
                error = _error
            };
            var res = new JsonResult(obj);
            return res;
        }

       
        public static string CalculateAge(DateTime? birthdate)
        {
            if (birthdate == null) return "-";
            // Save today's date.
            var today = DateTime.Today;
            // Calculate the age.
            var age = today.Year - birthdate.Value.Year;
            // Go back to the year the person was born in case of a leap year
            if (birthdate > today.AddYears(-age)) age--;
            return age.ToString();
        }
        
        
        public static async Task<byte[]> PostedFileToBytes(IFormFile file)
        {
            if (file == null || file.Length <= 0) return null;
            var resultFile = new byte[]{};
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                resultFile = stream.ToArray();
            }
            return resultFile;
        }

        public static string FileToBase64(byte[] file , DefaultEmptyFileName defaultEmptyFileName)
        {
            var d = _defaultEmptyFiles().FirstOrDefault(a => a.Key == defaultEmptyFileName.ToString()).Value;
            if (file != null && !(file?.Length <= 0))
                return $"data:image/gif;base64,{Convert.ToBase64String(file)}";

            if (d == null) return Empty;
            var filePath = "wwwroot/images/default/"+d;
            var binaryFile = File.ReadAllBytes(filePath);
            var base64 = Convert.ToBase64String(binaryFile);
            return $"data:image/gif;base64,{base64}";

        }
        
        public static string FileToBase64(byte[] file , string defaultEmptyFileName)
        {
            var d = _defaultEmptyFiles().FirstOrDefault(a => a.Key == defaultEmptyFileName.ToString()).Value;
            if (file != null && !(file?.Length <= 0))
                return $"data:image/gif;base64,{Convert.ToBase64String(file)}";

            if (d == null) return Empty;
            var filePath = "wwwroot/images/default/"+d;
            var binaryFile = File.ReadAllBytes(filePath);
            var base64 = Convert.ToBase64String(binaryFile);
            return $"data:image/gif;base64,{base64}";
        }
        
        private static IEnumerable<KeyValuePair<string, string>> _defaultEmptyFiles()
        {
            return new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("NoPhoto" ,"no-photo.jpg" ),
                new KeyValuePair<string, string>("NoImage" ,"no-image.jpg" ),
                new KeyValuePair<string, string>("NoReferee" ,"referee.png" ),
                new KeyValuePair<string, string>("NoClub" ,"no-club.png" ),
                new KeyValuePair<string, string>("NoPdf" ,"pdf.png" ),
            };
        }

        
         public static byte[] ResizeImageWithMaxDimension(Stream imageStream, int dim)
        {
            using (var image = SixLabors.ImageSharp.Image.Load(imageStream))
            {
                var height = image.Height;
                var width = image.Width;
                var maxDim = Math.Max(height, width);
                var scale = dim / (float) maxDim;
                imageStream.Position = 0;
                return ResizeImage(imageStream, scale);
            }
        }

        public static byte[] ResizeImageWithMaxDimension(byte[] imageBytes, int dim)
        {
            if(imageBytes == null || imageBytes.Length == 0) return new byte[]{};
            using (var stream = new MemoryStream(imageBytes))
            using (var image = SixLabors.ImageSharp.Image.Load(stream))
            {
                var height = image.Height;
                var width = image.Width;
                var maxDim = Math.Max(height, width);
                var scale = dim / (float) maxDim;
                stream.Position = 0;
                return ResizeImage(stream, scale);
            }
        }
        
        public static string ImageResizeToBase64(byte[] imageBytes, int dim)
        {
            if(imageBytes == null || imageBytes.Length == 0) return string.Empty;
            var binaryImage = new byte[]{};
            using (var stream = new MemoryStream(imageBytes))
            using (var image = SixLabors.ImageSharp.Image.Load(stream))
            {
                var height = image.Height;
                var width = image.Width;
                var maxDim = Math.Max(height, width);
                var scale = dim / (float) maxDim;
                stream.Position = 0;
                var task = Task.Run(() => { binaryImage= ResizeImage(stream, scale); });
                task.Wait();
                return FileToBase64(binaryImage, DefaultEmptyFileName.NoImage);
            }
        }
       
        public static byte[] ResizeImageWithMaxDimension(IFormFile imageFile, int dim)
        {
            if (imageFile?.Length > 0)
            {
                using (var stream = imageFile.OpenReadStream())
                using (var image = SixLabors.ImageSharp.Image.Load(stream))
                {
                    var height = image.Height;
                    var width = image.Width;
                    var maxDim = Math.Max(height, width);
                    var scale = dim / (float) maxDim;
                    stream.Position = 0;
                    return ResizeImage(stream, scale);
                }
            }
           return new byte[]{};
        }


        public static byte[] ResizeImage(Stream imageStream, float scale)
        {
            using (var image = SixLabors.ImageSharp.Image.Load(imageStream))
            {
                image.Mutate(x => x
                    .Resize((int) (image.Width * scale), (int) (image.Height * scale)));
                var base64String = image.ToBase64String(ImageFormats.Jpeg);
                return Convert.FromBase64String(base64String.Split(",")[1]);
            }
        }


        
    }
}