﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Matrix.Static
{
    public enum KpiResultCycleEnum
    {
        Annually,
        SemiAnnually,
        Quarterly
    }

    public enum Gender
    {
        Male,
        Female
    }
    
    public enum KpiCalculationMethodEnum
    {
        Split , 
        Repeat,
        Cumulative
    }
    public enum KpisType
    {
        Strategic , 
        Operational
    }

    
    public static class Enums
    {
        public static class StatusColor
        {
            public static string Normal = "progress-bar-default";
            public static string Danger = "progress-bar-danger";
            public static string Warning = "progress-bar-warning";
            public static string success = "progress-bar-success";
        }
    
        public  enum StatusEnum
        {
            pending, inprocess , rejected , approved
        }
        
        public  enum RefereePositions
        {
            FirstReferee,
            SecondReferee,
            ScorerReferee,
            ScorerAssistant,
            BorderReferee1,
            BorderReferee2,
            BorderReferee3,
            BorderReferee4
        }
        

        public static SelectList RefereePositionsToSelectList(object selected)
        {
            return new SelectList(
                Enum.GetValues(typeof(Enums.RefereePositions)).Cast<Enums.RefereePositions>().Select(v=>new SelectListItem()
                {
                    Text= MyStatics.FindResByName(v.ToString()) , Value = v.ToString()
                }).ToList(), "Value", "Text" , selected); 
        }
        public static SelectList RefereePositionsToSelectList()
        {
            return new SelectList(
                Enum.GetValues(typeof(Enums.RefereePositions)).Cast<Enums.RefereePositions>().Select(v=>new SelectListItem()
                {
                    Text= MyStatics.FindResByName(v.ToString()) , Value = v.ToString()
                }).ToList(), "Value", "Text"); 
        }
        
        // default empty files to be replaced when orginal file is not exists
            
    }
}