﻿using System;
using System.ComponentModel;

namespace Cid_Criminal.Models
{
    public class SearchModel
    {
        public Matrix.Data.DomainClasses.Profile Profile { get; set; }

        [DisplayName("العمر")]
        public int Age { get; set; }

        [DisplayName("الجنس")]
        public int GenderId { get; set; }

        [DisplayName("الملامح القارية")]
        public int CountenanceId { get; set; }

        [DisplayName("اوصاف المتهم")]
        public int CountenanceDesc { get; set; }

        [DisplayName("التصنيف الاجرامي")]
        public int CriminalCategoryId { get; set; }

        [DisplayName("الجنسية")]
        public int NationalityId { get; set; }

        [DisplayName("دورية الاختصاص")]
        public int CallID { get; set; }

        [DisplayName("المنطقة")]
        public int AreaId { get; set; }
        
        [DisplayName("مكان الحادث")]
        public int Place { get; set; }

        [DisplayName("التكييف القانوني")]
        public int CaseId { get; set; }

        [DisplayName("الأسلوب الإجرامي")]
        public int CrimeMethodId { get; set; }

        [DisplayName("ملخص الأسلوب ")]
        public int MethodDescription { get; set; }

        [DisplayName("نوع الجريمة")]
        public int CrimeTypeId { get; set; }

        [DisplayName("نوع الجريمة الفرعي")]
        public int SubCrimeTypeId { get; set; }

        [DisplayName("المركز")]
        public int DepartmentId { get; set; }

        [DisplayName("تاريخ البلاغ")]
        public DateTime? CrimeDate { get; set; }

        [DisplayName("الوقت من")]
        public DateTime? TimeFrom { get; set; }

        [DisplayName("الوقت الى")]
        public DateTime? TimeTo { get; set; }

        [DisplayName("تشكيل عصابي")]
        public bool IsGang { get; set; }


        [DisplayName("عنوان الشخص")]
        public string GangName { get; set; }

        [DisplayName("عنوان الشخص")]
        public string CriminalAddress { get; set; }

        [DisplayName("الإمارة")]
        public int CityID { get; set; }

        [DisplayName("اسم الشخص")]
        public string CriminalName { get; set; }
        
        [DisplayName("اسم الشهرة")]
        public string CriminalNickName { get; set; }
        [DisplayName("نوع مسرح الجريمة")]
        public int PlaceTypeID { get; set; }        
        [DisplayName("حالة مسرح الجريمة")]
        public int PlaceStatusID { get; set; }

        public string CriminalDescription { get; set; }

        [DisplayName("علامه مميزه")]
        public int CriminalLook { get; set; }
        
        
        [DisplayName("ملاحظات البلاغ")]
        public string Notes { get; set; }

        [DisplayName("رقم الموبايل")]
        public string Mobile { get; set; }

        [DisplayName("الدافع من الجريمة")]
        public string CrimeMotive { get; set; }

        public Int64? UNID { get; set; }

        public Int64? FingerprintNumber { get; set; }

        [DisplayName("نوع الاجراء")]
        public int RecordTypeID { get; set; }

        [DisplayName("مکانی")]
        public string Makani { get; set; }
        [DisplayName("ملاحظات")]
        public string Remarks { get; set; }
        

        [DisplayName("النوع")]
        public string Types { get; set; }
        [DisplayName("الفئة")]
        public string Category { get; set; }
        [DisplayName("التفصيل")]
        public string Details { get; set; }
        [DisplayName("الوصف عربي")]
        public string Description_Arabic { get; set; }
        [DisplayName("القيمة")]
        public string valuess { get; set; }
        [DisplayName("الكمية")]
        public string Quantity { get; set; }
        [DisplayName("دور الشيء")]
        public string Role_Of_things { get; set; }
    }

    public class SearchForUSerAuditL1
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public Int32 Cases { get; set; }

        
    }

    public class SearchForUSerAuditL2
    {
        public Int32 RefID { get; set; }
        public string RefType { get; set; }
        public Int32 NoOfActivities { get; set; }
        public string Details { get; set; }


    }
    public class SearchForVisitorAudit
    {
        public string UserID { get; set; }
        public Int32 EmpID { get; set; }
        public string RefType { get; set; }
        public string UserName { get; set; }
        public string TraceRemarks { get; set; }
        public DateTime DateAndTime { get; set; }


    }
   
    public class SreachForCriminalResults
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Int64? UNID { get; set; }
        public Int64? FingerprintNumber { get; set; }
        public string NickName { get; set; }
        public string Nationality { get; set; }
        public decimal Age { get; set; }
        public int TotalCrimes { get; set; }
        public string CriminalCategory { get; set; }
        public string Countenance { get; set; }
        public string Gender { get; set; }
          
    }

    public class SreachForRecordsResults
    {
        public int Id { get; set; }
        public DateTime RecordDate { get; set; }
        public int Number { get; set; }
        public string DepName { get; set; }
        public string Rec_Status { get; set; }
        public string AreaName { get; set; }
        public string CriminalType { get;set;}
        public string SubCriminalType { get; set; }
        public string CriminalMethod { get; set; }
        [DisplayName("تاريخ ورود البلاغ")]
        public DateTime RecordDate1 { get; set; }
        [DisplayName("تاريخ الحادث")]
        public DateTime? CrimeDate { get; set; }
        [DisplayName("وقت الحادث")]
        public TimeSpan? TimeFrom { get; set; }
        public TimeSpan? TimeTo { get; set; }
        [DisplayName("للتهمة التكيف القانوني")]
        public string OffenseCharge { get; set; }
        public string CallNumber { get; set; }
        public string Place { get; set; }
        public decimal? StolenValue { get; set; }
        [DisplayName("الادوات المستخدمة")]
        public string ToolName { get; set; }
        [DisplayName("الآثار المتخلفة")]
        public string ImpactName { get; set; }
        public string CriminalDescription { get; set; }
        public string Notes { get; set; }
        public string MethodDescription { get; set; }
        [DisplayName("النوع")]
        public string Types { get; set; }
        [DisplayName("الفئة")]
        public string Category { get; set; }
        [DisplayName("التفصيل")]
        public string Details { get; set; }
        [DisplayName("الوصف عربي")]
        public string Description_Arabic { get; set; }
        [DisplayName("القيمة")]
        public string valuess { get; set; }
        [DisplayName("الكمية")]
        public string Quantity { get; set; }
        [DisplayName("دور الشيء")]
        public string Role_Of_things { get; set; }

    }

    public class StolenCrimesSearch
    {

        [DisplayName("المركز")]
        public int DepartmentId { get; set; }
        [DisplayName("رقم البلاغ")]
        public int CaseNumber { get; set; }
        [DisplayName("السنه")]
        public int CaseYear { get; set; }
        [DisplayName("رقم المعاملة")]
        public int FormNumber { get; set; }
        [DisplayName("اسم المسروق")]
        public string StolenName { get; set; }
        
        [DisplayName("اسم المبلغ")]
        public string ComplainerName { get; set; }

        [DisplayName("وصف اضافي")]
        public string Description { get; set; }

        [DisplayName("القيمة الاجمالية")]
        public int TotalValue { get; set; }

        [DisplayName("الكمية")]
        public int Quantity { get; set; }

        [DisplayName("حالة ضبط المسروق")]
        public bool IsCaptuered { get; set; }
    }

    public class StolenCrimesSearchResult
    {
        public int ID { get; set; }

        [DisplayName("المركز")]
        public string DepName { get; set; }
        
        [DisplayName("رقم البلاغ")]
        public string CrimeNumber { get; set; }
     
        [DisplayName("تاريخ التعميم")]
        public string CrimeDate { get; set; }

        [DisplayName("التصنيف")]
        public string CatName { get; set; }

        [DisplayName("المسروق")]
        public string ItemName { get; set; }

        [DisplayName("الكمية")]
        public Nullable<int> Quantity { get; set; }

        [DisplayName("القيمة")]
        public Nullable<decimal> ItemValue { get; set; }

        
      
        
    }
}