﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;

namespace Cid_Criminal.Models
{
    public class ImagesLib
    {
        public  Bitmap ResizeImage(Image image, int width)
        {
            int w = width;
            // Create a bitmap of the content of the fileUpload control in memory
            Bitmap originalBMP = new Bitmap(image);

            // Calculate the new image dimensions
            int origWidth = originalBMP.Width;
            int origHeight = originalBMP.Height;
            int sngRatio = (origWidth / origHeight) < 1 ? 1 : (origWidth / origHeight);
            int newWidth = width;
            int newHeight = newWidth / sngRatio;

            // Create a new bitmap which will hold the previous resized bitmap
            Bitmap newBMP = new Bitmap(originalBMP, newWidth, newHeight);
            // Create a graphic based on the new bitmap
            Graphics oGraphics = Graphics.FromImage(newBMP);

            // Set the properties for the new graphic file
            oGraphics.SmoothingMode = SmoothingMode.AntiAlias; 
            oGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            // Draw the new graphic based on the resized bitmap
            oGraphics.DrawImage(originalBMP, 0, 0, newWidth, newHeight);

            originalBMP.Dispose();
            oGraphics.Dispose();  
            return newBMP;

        }

        public byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            imageIn.Dispose();
            return ms.ToArray();
        }
    }
}