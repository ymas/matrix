﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Matrix.Data.DomainClasses;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace Matrix.Models.IdentityModels
{
    public  class AppClaimsPrincipalFactory : UserClaimsPrincipalFactory<ApplicationUser, Role>
    {

        public AppClaimsPrincipalFactory(
            UserManager<ApplicationUser> userManager,
            RoleManager<Role> roleManager,
            IOptions<IdentityOptions> optionsAccessor) : base(userManager, roleManager, optionsAccessor)
        {
        }

        public override async Task<ClaimsPrincipal> CreateAsync(ApplicationUser user)
        {
            var principal = await base.CreateAsync(user);
            

                ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                    new Claim("ArabicName", 
                            string.IsNullOrEmpty(user.NameAr)
                                ? user.FullName.Split(' ').First()
                                : user.NameAr.Split(' ').First())
                });
                
                ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                    new Claim("EnglishName", user.FullName.Split(' ').First())
                });
            
                
                ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                    new Claim("IsEnglishLanguage", user?.IsEnglishLanguage.ToString() ?? "false")
                });
            
            if (user.EmployeeNo != null)
            {
                ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                    new Claim("EmployeeNo", user.EmployeeNo.ToString())
                });
            }
        
                ((ClaimsIdentity)principal.Identity).AddClaims(new[] {
                    new Claim("IsSuperAdmin", "false")
                });

            return principal;
        }
    
    
    }
}
