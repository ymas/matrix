﻿using Microsoft.AspNetCore.Authorization;

namespace System
{
    public class RolesAttribute : AuthorizeAttribute
    {

        public RolesAttribute(params string[] roles)
        {
            Roles = string.Join(",", roles);
        }
    }

}