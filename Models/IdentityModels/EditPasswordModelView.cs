﻿using System;

namespace Matrix.Models
{
    public class EditPasswordModelView
    {
        public Guid UserID { get; set; }
        public string Password { get; set; }
    }
}