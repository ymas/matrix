﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Matrix.Models
{
   
 
    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "User Name")]
        public string Email { get; set; }
    }


    public class RolesCheckbox
    {
        public Guid RoleId { get; set; }
        public string Name { get; set; }
        public string RoleName { get; set; }
        public string Checked { get; set; }
    }



 
}
