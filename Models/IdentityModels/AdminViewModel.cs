﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Matrix.Resources;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Matrix.Models
{
    public class RoleCheckBoxViewModel
    {
       
        public string Name { get; set; }
        public Guid ID { get; set; }
        public string Desc { get; set; }
    }
    public class RoleViewModel
    {
        public Guid Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleName")]
        public string Name { get; set; }

        public int ProfileID { get; set; }


        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleDescription" , ResourceType =typeof(_Translate))]
        public string RoleName { get; set; }
    }

    public class EditUserViewModel
    {
        public Guid Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "UserName")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "EmployeeId")]
        public Guid EmployeeId { get; set; }
        
        
        [Required]
        [Display(Name = "FullName")]
        public string FullName { get; set; }

        
        public IEnumerable<SelectListItem> RolesList { get; set; }
    }

    public class UsersIndex {
        public string Id { get; set; }
        public int EmployeeId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string DepName  { get; set; }
        public int TotalEntry { get; set; }
    } 
}