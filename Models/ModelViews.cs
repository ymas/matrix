﻿namespace Matrix.Data.DomainClasses
{
    public class ProfileDetailsGetMethods
    {
        public int TotalCrimes { get; set; }
        public string Name { get; set; }
    }


    //Crimina Partners

    public class Criminapartners
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Nationality { get; set; }
    }
}