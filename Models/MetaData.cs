﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Cid_Criminal.Models
{
    public class MetaData
    {
    }

    public partial class ProfilePhoto
    {
        public HttpPostedFileBase PhotoFile { get; set; }
    }

    [MetadataType(typeof(ProfileMeta))]
    public partial class Profile
    {
        private CriminalEntities db = new CriminalEntities();

        [Display(Name = "الصورة")]
        public HttpPostedFileBase PhotoFile { get; set; }

        [Display(Name = "الإمارة")]

        public string CityName
        {
            get
            {
                if (this.CityID > 0)
                {
                    return db.cities.Find(this.CityID).Name;
                }
                else
                {
                    return "غير محدد";
                }
            }

        }

        [Display(Name = "منطقة")]
        public string AreaName
        {
            get
            {
                if (this.AreaID > 0)
                {
                    return db.Areas.Find(this.AreaID).Name;
                }
                else
                {
                    return "غير محدد";
                }
            }

        }

        [Display(Name = "وضعية الشخص")]

        public string StatusName
        {
            get
            {
                if (!string.IsNullOrEmpty(this.StatusFlag))
                {
                    var s = StatusFlagList.getList();
                    return s.Where(a => a.StatusID == this.StatusFlag).Select(a => a.StatusName).FirstOrDefault();
                }
                else
                {
                    return "غير محدد";
                }
            }

        }
        [Display(Name = "الجنس")]

        public string GenderName
        {
            get
            {
                if (this.Gender == "f")
                {
                    return "انثى";
                }
                else
                {
                    return "ذكر";
                }
            }

        }

        [Display(Name = "عدد الجرائم")]
        public int TotalCrimes
        {
            get
            {
                return (this.Crime_Profile.Where(a => a.Crime.IsDeleted == false && a.ProfileType == "متهم").Any()
                       ? this.Crime_Profile.Where(a => a.Crime.IsDeleted == false && a.ProfileType == "متهم").Count()
                       : 0);
            }
        }

        [Display(Name = "العمر")]
        public int Age
        {
            get
            {
                if (this.DateOfBirth != null && this.DateOfBirth < DateTime.Today)
                {
                    int nowYear = DateTime.Today.Year;
                    int borneYear = this.DateOfBirth.Value.Year;
                    int age = nowYear - borneYear;
                    return age;
                }
                return 0;
            }
        }
    }

    public class ProfileMeta
    {


        [Display(Name = "الاسم")]
        public string Name { get; set; }
        [Display(Name = "الجنسية")]
        public int NationalityId { get; set; }

        [Display(Name = "رقم الموبايل")]
        public string Mobile { get; set; }

        [Display(Name = "الرقم الموحد")]
        public int UNID { get; set; }

        [Display(Name = "تاريخ الميلاد")]
        public Nullable<System.DateTime> DateOfBirth { get; set; }

        [Display(Name = "رقم البصمة")]
        public Nullable<long> FingerprintNumber { get; set; }
        [Display(Name = "التصنيف الاجرامي")]
        public Nullable<int> CriminalCategoryId { get; set; }
        [Display(Name = "اسم الشهرة")]
        public string Nickname { get; set; }
        [Display(Name = "الملامح القارية")]
        public int CountenanceId { get; set; }
        [Display(Name = "المهنة")]
        public string Job { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "العنوان")]
        public string Address { get; set; }
        [Display(Name = "الجنس")]
        public string Gender { get; set; }

        [Display(Name = "الصورة")]
        public byte[] Photo { get; set; }

        [Display(Name = "الإمارة")]
        public Nullable<int> CityID { get; set; }

        [Display(Name = "وضعية الشخص")]
        public string StatusFlag { get; set; }

        [Display(Name = "")]
        public Nullable<int> AreaID { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Name = "ملاحظات")]
        public string Remarks { get; set; }

        [Display(Name = "مکانی")]
        public string Makani { get; set; }
    }



    [MetadataType(typeof(CrimeMeta))]
    public partial class Crime
    {

        [Display(Name = "حالة البلاغ")]
        public string Status
        {
            get
            {
                return (this.Crime_Profile.Where(a => a.ProfileType == "متهم").Any() ? "معلوم" : "مجهول");
            }
        }
    }

    public class CrimeMeta
    {


        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "المركز")]
        public int DepartmentId { get; set; }

        [Display(Name = "رقم البلاغ")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Number { get; set; }


        [Display(Name = "الدافع من الجريمة")]
        public string CrimeMotive { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "تاريخ البلاغ")]
        public System.DateTime RecordDate { get; set; }


        //[Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "تاريخ الحادث")]
        public System.DateTime CrimeDate { get; set; }


        [Display(Name = "من")]
        public Nullable<System.TimeSpan> TimeFrom { get; set; }
        [Display(Name = "الى")]

        public Nullable<System.TimeSpan> TimeTo { get; set; }


        [Display(Name = "التكييف القانوني")]
        //[Required(ErrorMessage = "حقل اجباري")]
        public Nullable<int> CaseId { get; set; }


        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "نوع الجريمة")]
        public int CrimeTypeId { get; set; }

        [Display(Name = "التصنيف الفرعي للجريمة")]
        public int SubCrimeTypeId { get; set; }

        [Display(Name = "دورية الاختصاص")]
        public string CallNumber { get; set; }


        // [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "منطقة الحادث ")]
        public int AreaId { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "مسرح الجريمة ")]
        public int PlaceTypeID { get; set; }

        [Display(Name = "وصف مسرح الجريمة ")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string Place { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [DisplayName("حالة المكان")]
        public string PlaceStatusID { get; set; }


        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "الاسلوب الاجرامي")]
        public int CrimeMethodId { get; set; }


        [Display(Name = "خطوات تنفيذ الاسلوب")]
        public int CrimeStepsDescription { get; set; }


        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "ملخص الاسلوب")]
        [DataType(DataType.MultilineText)]
        public string MethodDescription { get; set; }


        [Display(Name = "المخطط الاجرامي")]
        public string CriminalPlanID { get; set; }



        [Display(Name = "علامه مميزه")]
        public string CriminalLook { get; set; }


        [Display(Name = "اوصاف المتهم")]
        public string CriminalDescription { get; set; }



        [Display(Name = "قيمة المسروقات")]
        public Nullable<decimal> StolenValue { get; set; }

        [Display(Name = "ملاحظات")]
        public string Notes { get; set; }

        [Display(Name = "النوع")]
        public string Types { get; set; }

        [Display(Name = "الفئة")]
        public string Category { get; set; }

        [Display(Name = "التفصيل")]
        public string Details { get; set; }

        [Display(Name = "الوصف عربي")]
        public string Description_Arabic { get; set; }

        [Display(Name = "القيمة")]
        public string valuess { get; set; }

        [Display(Name = "الكمية")]
        public string Quantity { get; set; }

        [Display(Name = "دور الشيء")]
        public string Role_Of_things { get; set; }


        [Display(Name = "آخر مرة تم التحديث بواسطة")]
        public string CreatedBy { get; set; }


        [Display(Name = "تشكيل عصابي")]
        public bool IsGang { get; set; }


        [Display(Name = "اسم العصابة")]
        public string GangName { get; set; }

        [Display(Name = "حالة البلاغ")]
        public bool IsKnown { get; set; }

        [Display(Name = "نوع العملة")]
        public string CurrencyName { get; set; }

        [Display(Name = "الفئة")]
        public string CurrencyType { get; set; }

        [Display(Name = "المبلغ المضبوط")]
        public Nullable<decimal> CurrencyTotal { get; set; }

    }

    [MetadataType(typeof(StolenItemMeta))]
    public partial class StolenItem
    {
        [Display(Name = "الصورة")]
        public HttpPostedFileBase ImageFile { get; set; }
    }


    public class StolenItemMeta
    {
        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "تصنيف المسروق")]
        public int ItemCategoryId { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "اسم المسروق")]
        public string Name { get; set; }

        [Display(Name = "وصف اضافي")]
        public string Description { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "القيمة ")]
        public decimal ValuePerItem { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "الكمية")]
        public int Quantity { get; set; }

        [Display(Name = "تم ضبط المسروق")]
        public bool IsCaptuered { get; set; }

        [Display(Name = "صورة المسورقات ")]
        public byte[] Image { get; set; }
    }

    [MetadataType(typeof(StealingCrimeMeta))]
    public partial class StealingCrime
    {

    }

    public class StealingCrimeMeta
    {
        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "المركز")]
        public int DepartmentId { get; set; }

        [DisplayName("رقم البلاغ")]
        public string CaseNumber { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "رقم المعاملة")]
        public string FormNumber { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "رقم الوارد")]
        public string NoticeNumber { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "تاريخ التعميم")]
        public System.DateTime NoticeDateAndTime { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "اسم المبلغ")]
        public string ComplainerName { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "الجنسية")]
        public int ComplainerNationality { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "تاريخ الاستلام")]
        public System.DateTime ReceptionDate { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "تاريخ الرد")]
        public System.DateTime ReplyDate { get; set; }

        [Required(ErrorMessage = "حقل اجباري")]
        [Display(Name = "جهة التحويل")]
        public string TransferDestination { get; set; }

        [Display(Name = "رقم صادر ")]
        public string TransferNumber { get; set; }

        [Display(Name = "تاريخ الاعتماد")]
        public Nullable<System.DateTime> ApprovalDate { get; set; }
    }



    //Case_Status
    [MetadataType(typeof(Case_Status_Meta))]
    public partial class Case_Status { }
    public partial class Case_Status_Meta
    {
        [DisplayName(" حالة البلاغ")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string Name { get; set; }
    }


    //Sub Crime Type
    [MetadataType(typeof(SubCrimeType_Meta))]
    public partial class SubCrimeType { }
    public partial class SubCrimeType_Meta
    {
        [DisplayName(" التصنيف الفرعي للجريمة")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string Name { get; set; }

        [DisplayName("نوع الجريمة")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int CrimeTypeID { get; set; }
    }


    //Record_Effects
    [MetadataType(typeof(Effects_Remains_Meta))]
    public partial class Effects_Remains { }
    public partial class Effects_Remains_Meta
    {
        [DisplayName(" الاثر")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string Name { get; set; }
    }


    //place type
    [MetadataType(typeof(PlaceType_Meta))]
    public partial class PlaceType { }
    public partial class PlaceType_Meta
    {
        [DisplayName(" نوع مسرح الجريمة")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string Name { get; set; }
    }



    //Record_Things
    [MetadataType(typeof(Record_Things_Meta))]
    public partial class Record_Things { }
    public partial class Record_Things_Meta
    {
        [DisplayName("المسروقات")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int ThingID { get; set; }

        public int RecordID { get; set; }
        [DisplayName("الكمية")]
        [Required(ErrorMessage = "حقل اجباري")]

        public int Quantity { get; set; }
        [DisplayName("القيمة الاجمالية")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string TotalPrice { get; set; }

        [DisplayName("ملاحظات")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string Notes { get; set; }
    }




    //Nationality
    [MetadataType(typeof(Nationality_Meta))]
    public partial class Nationality { }
    public partial class Nationality_Meta
    {
        [DisplayName("الجنسية")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }

    [MetadataType(typeof(dep_Meta))]
    public partial class dep { }
    public partial class dep_Meta
    {
        [DisplayName("المركز")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int DepName { get; set; }
    }

    [MetadataType(typeof(CriminalType_Meta))]
    public partial class CriminalType { }
    public partial class CriminalType_Meta
    {
        [DisplayName("نوع الجريمة")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }

    [MetadataType(typeof(CriminalCategory_Meta))]
    public partial class CriminalCategory { }
    public partial class CriminalCategory_Meta
    {
        [DisplayName("التصنيف الاجرامي")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }


    [MetadataType(typeof(Criminal_Method_Meta))]
    public partial class Criminal_Method { }
    public partial class Criminal_Method_Meta
    {
        [DisplayName("الأسلوب الإجرامي")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }



    [MetadataType(typeof(PlaceStatu_Meta))]
    public partial class PlaceStatu { }
    public partial class PlaceStatu_Meta
    {
        [DisplayName("حالة مسرح الجريمة")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }



    [MetadataType(typeof(City_Meta))]
    public partial class City { }
    public partial class City_Meta
    {
        [DisplayName("الإمارة")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }






    [MetadataType(typeof(Countenance_Meta))]
    public partial class Countenance { }
    public partial class Countenance_Meta
    {
        [DisplayName("الملامح القارية")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }



    [MetadataType(typeof(Area_Meta))]
    public partial class Area { }
    public partial class Area_Meta
    {
        [DisplayName("منطقة الحادث")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }

    [MetadataType(typeof(Tool_Meta))]
    public partial class Tool { }
    public partial class Tool_Meta
    {
        [DisplayName("الأداه")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }


    [MetadataType(typeof(StolenDisposal_Meta))]
    public partial class StolenDisposal { }
    public partial class StolenDisposal_Meta
    {
        [DisplayName("طريقة التصريف")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }


    [MetadataType(typeof(CrimeRecord_Type_Meta))]
    public partial class CrimeRecord_Type { }
    public partial class CrimeRecord_Type_Meta
    {
        [DisplayName("النوع")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int Name { get; set; }
    }


    [MetadataType(typeof(Crime_ProfileMeta))]
    public partial class Crime_Profile
    {

        [DisplayName("حالة الضبط")]
        public string ArrestedStatusName
        {
            get
            {
                if (this.ArrestedStatus == 0)
                {
                    return "هارب";
                }
                else
                {
                    return "مضبوط";
                }
            }
        }
    }
    public partial class Crime_ProfileMeta
    {

        [DisplayName("ملف الشخص")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int ProfileId { get; set; }

        [DisplayName("ملف البلاغ")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int CrimeId { get; set; }

        [DisplayName("نوع العلاقة")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string ProfileType { get; set; }

        [DisplayName("الجنسية")]
        public int NationalityId { get; set; }

        [DisplayName("حالة الضبط")]
        public Nullable<int> ArrestedStatus { get; set; }

        [DisplayName("جهة الضبط")]
        public string ArrestedBy { get; set; }

        [DisplayName("تاريخ الضبط")]
        public Nullable<System.DateTime> ArrestedDate { get; set; }

    }


    [MetadataType(typeof(CrimeRecord_Meta))]
    public partial class CrimeRecord
    {
        private CriminalEntities db = new CriminalEntities();

        public string CreatedByName
        {
            get
            {
                return (db.UserInfoes.Where(a => a.UserID == this.CreatedBy) != null ? db.UserInfoes.Where(a => a.UserID == this.CreatedBy).First().UserName : "غير محدد");
            }

        }
    }
    public partial class CrimeRecord_Meta
    {

        [DisplayName("تاريخ الادخال")]
        public Nullable<System.DateTime> CreatedDate { get; set; }

        [DisplayName("بواسطة")]
        public string CreatedBy { get; set; }

        [DisplayName("التاريخ")]
        [Required(ErrorMessage = "حقل اجباري")]
        public System.DateTime RecordDate { get; set; }

        [DisplayName("نوع الاجراء")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int RecordTypeID { get; set; }

        [DisplayName("التفاصيل")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string Description { get; set; }

        [DisplayName("المرفق")]
        public byte[] Attachment { get; set; }

        [DisplayName("البلاغ")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int CrimeID { get; set; }
    }



    [MetadataType(typeof(StolenItemsDisposal_Meta))]
    public partial class StolenItemsDisposal
    {
        //  private CriminalEntities db = new CriminalEntities();

    }
    public partial class StolenItemsDisposal_Meta
    {
        [DisplayName("المسروق")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int StolenItemID { get; set; }

        [DisplayName("طريقة التصريف")]
        [Required(ErrorMessage = "حقل اجباري")]
        public int StoenDisposalID { get; set; }

        [DisplayName("تفاصيل ")]
        [Required(ErrorMessage = "حقل اجباري")]
        public string DisposalDesc { get; set; }

    }


    public class StatusFlag
    {
        public string StatusID { get; set; }
        public string StatusName { get; set; }
    }

    public static class StatusFlagList
    {
        public static List<StatusFlag> getList()
        {
            var _list = new List<StatusFlag>();
            _list.Add(new StatusFlag() { StatusID = "active", StatusName = "طليق" });
            _list.Add(new StatusFlag() { StatusID = "died", StatusName = "متوفي" });
            _list.Add(new StatusFlag() { StatusID = "out", StatusName = "خارج الدولة" });
            _list.Add(new StatusFlag() { StatusID = "prisoner", StatusName = "موقوف" });
            return _list;
        }

    }





}