﻿namespace Matrix.Data.DomainClasses
{
    public class NameCount 
    {
        public string Name { get; set; }
        public int? Count { get; set; }
    }
}