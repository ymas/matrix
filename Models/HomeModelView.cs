﻿namespace Matrix.Models
{
    public class TotalEntriesForEachMainCat
    {
        public int TotalEntries { get; set; }
        public int CatID { get; set; }
        public string CatnName { get; set; }
        public double Percentage { get; set; }
        public int TotalSubCat { get; set; }
        public int TotalActivities { get; set; }
        public int TotalForms { get; set; }
    }

    public class TopEnterer
    {
        public string Name { get; set; }
        public int Count { get; set; }
    }

    public class TopActiveDep
    {
        // persent from the total
        public double TotalEntries { get; set; }
        public string DebName { get; set; }
    }

    public class TopLeader
    {
        // persent from the total
        public string LeaderName { get; set; }
        public int TotalEntries { get; set; }
        public int Id { get; set; }
        public string Dep { get; set; }

    }

}