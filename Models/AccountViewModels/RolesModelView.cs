namespace Matrix.Models.AccountViewModels
{
    public class CreateRolesModelView
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}