﻿using System.ComponentModel.DataAnnotations;

namespace Matrix.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "InsertValidUserName" )]
        [Display( Name="UserName")]
        public string Email { get; set; }

        [Required(ErrorMessage = "InsertValidPassword" )]
        [Display(Name="Password")]        
        [DataType(DataType.Password)]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}
