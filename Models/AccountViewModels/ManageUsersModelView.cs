﻿using System;

namespace Matrix.Models.AccountViewModels
{
    public class ManageUsersModelView
    {
        public Guid? EmployeeId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string RegisterDate { get; set; }
        public string Roles { get; set; }
        public string Permissions { get; set; }
    }
}