﻿using System.ComponentModel.DataAnnotations;

namespace Matrix.Models.AccountViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User Name")]
        public string Email { get; set; }
    }
}