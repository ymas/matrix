﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Matrix.Data.DomainClasses;
using Matrix.Static;
using Microsoft.AspNetCore.Http;

namespace Matrix.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [DataType(DataType.Password)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public Gender Gender { get; set; }
        
        [Display(Name = "Photo")]
        public IFormFile Photo { get; set; }
        
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[Required(ErrorMessage="Insert user name.")]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Insert full name in english")]
        [Display(Name = "FullName")]
        public string FullName { get; set; }
        
        [Required(ErrorMessage = "Insert full name in arabic")]
        public string NameAr { get; set; }

        [Display(Name = "EmployeeId")]
        //[Required(ErrorMessage = "Insert staff ID number.")]
        public int? EmployeeNo { get; set; }

        [Required(ErrorMessage = "Please select the access role.")]
        [Display(Name = "AccessRoles")]
        public string[] Roles { get; set; }

        public List<Role>  RolesList { get; set; }

    }
}
