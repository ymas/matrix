﻿namespace Matrix.Models.AccountViewModels
{
    public class UsersList {
        public int? EmployeeId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string FullName { get; set; }
        public string RegisterDate { get; set; }
        public string Roles { get; set; }
    }
}