﻿using System.ComponentModel.DataAnnotations;

namespace Matrix.Models.AccountViewModels
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "EnterYourUserName" )]
        [EmailAddress]
        [Display(Name = "UserName" )]
        public string Email { get; set; }
    }
}
