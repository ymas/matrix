﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Matrix.Static;
using Microsoft.AspNetCore.Http;

namespace Matrix.Models.AccountViewModels
{
    public class EditRegisterViewModel
    {
        public Guid Id { get; set; }

        [Required]
        [EmailAddress]
        [DataType(DataType.Password)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required]
        [Display(Name = "Gender")]
        public Gender Gender { get; set; }
        
        [Display(Name = "Photo")]
        public IFormFile Photo { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        //[Required(ErrorMessage ="Insert username")]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Insert full name in english")]
        [Display(Name = "FullName")]
        public string FullName { get; set; }
        
        [Required(ErrorMessage = "Insert full name in arabic")]
        public string NameAr { get; set; }

        [Display(Name = "EmployeeId")]
        //[Required(ErrorMessage = "Insert staff ID ")]
        public int? EmployeeNo { get; set; }

        [Required(ErrorMessage = "Select user role")]
        [Display(Name = "AccessRoles")]
        public string[] Roles { get; set; }

        public List<RolesCheckbox> SelectedRoles { get; set; }
    }
}