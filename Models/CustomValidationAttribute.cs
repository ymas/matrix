﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Matrix.Models
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class GraterThan : ValidationAttribute
    {
        public int _Val { get; set; }
        public GraterThan(int graterThan)
        {
            this._Val = graterThan;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int val = (int)value;

            if (val > _Val)
                return ValidationResult.Success;
            else
                return new ValidationResult(ErrorMessageString);
        }
    }
}