﻿namespace Cid_Criminal.Models
{
    public class ChartData
    {
        public string label { get; set; }
        public int value { get; set; }
    }
    public class HourCountChartModel
    {
        public int Hour { get; set; }
        public int Count { get; set; }
    }
}