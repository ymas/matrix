﻿namespace Matrix.Models
{
    public class JsonRes
    {
        public int success { get; set; }
        public string message { get; set; }
        public object extra { get; set; }

        public JsonRes(int success, string message, object extra)
        {
            this.success = success;
            this.message = message;
            this.extra = extra;
        }
    }
}