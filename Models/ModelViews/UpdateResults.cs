using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Matrix.Models.ModelViews
{
    public class UpdateResults
    {
        public Guid? Id { get; set; }

        public int[] AllowedYears { get; set; }

        [Required]
        public Guid KpiId { get; set; }
        
        [Required]
        [DisplayName("Year")]
        public int Year { get; set; }
        
        //enum KpiResultCycle
        [Display(Name = "ResultCycle")]
        public string ResultCycle { get; set; } /*annually , semi-annuall quarterly*/
        
        
        public string CalculationMethod { get; set; } 


        [Required]
        [DisplayName("Target")]
        public string Target { get; set; }
        
        [Required]
        [DisplayName("Actual")]
        public string Actual { get; set; }

        [DisplayName("Direction")]
        public string KpiName { get; set; }
        public string Direction { get; set; }

        public string Reasoning { get; set; }
        public string Benchmarking { get; set; }
        public string Opportunities4Improvement { get; set; }

        public string LastUpdateNote { get; set; }
        
        public string Unit { get; set; }

        public string KpiType { get; set; }
        
        public List<QuarterResultModelView> QuarterlyResults { get; set; }
        
        
        // this properties to hold the post data
        public double? Target1 { get; set; }
        public double? Target2 { get; set; }
        public double? Target3 { get; set; }
        public double? Target4 { get; set; }
        
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public string Value4 { get; set; }
       
    }

    public class QuarterResultModelView
    {
        public int Quarter { get; set; }
        public double Target { get; set; }
        public string Value { get; set; }

        public QuarterResultModelView(int quarter, double target, string value)
        {
            Quarter = quarter;
            Target = target;
            Value = value;
        }
        
    }
}