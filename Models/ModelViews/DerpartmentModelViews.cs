﻿using System;
using System.Collections.Generic;

namespace Matrix.Models
{
    public class DivisionDropList
    {
        public Guid Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string ParentAr { get; set; }
        public string ParentEn { get; set; }
        public string SectionName => Utilties.TransableName(NameAr,NameEn);
        public string DepartmentName => Utilties.TransableName(ParentAr,ParentEn);
    }

    public class HierarchicalDepartments
    {
        public Guid Id { get; set; }
        public Guid? ParentId { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public int DepLevel { get; set; }
        public string DepType { get; set; }
    }

    public class DepList
    {
        public Guid Id { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public Guid? ParentId { get; set; }
        public ICollection<DepList> ChildDepartments { get; set; }
    }
    
}