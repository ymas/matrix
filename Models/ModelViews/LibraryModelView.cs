﻿using System;

namespace Matrix.Models.ModelViews
{
    public class LibraryModelView
    {
        public Guid Id { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public string FileMime { get; set; }
        public string LastUpDateTime { get; set; }
        public int  TotalLinks { get; set; }
        public bool IsPrivate { get; set; }
        public Guid OwnerId { get; set; }
        public string OwnerName { get; set; }

        public LibraryModelView()
        {
        }

        public LibraryModelView(Guid? id , string name , string fileSize , string fileMime , DateTime? lasDateTime , int? totalLinks , bool isPrivate , Guid? ownerId , string ownerName)
        {
            Id = ownerId ?? Guid.Empty;
            FileName = name;
            FileSize = fileSize;
            FileMime = fileMime;
            LastUpDateTime = lasDateTime?.ToLongTimeString() ?? DateTime.Now.AddDays(-(new Random().Next(1,20))).ToLongDateString();
            IsPrivate = isPrivate;
            OwnerId = ownerId ?? Guid.Empty;
            OwnerName = ownerName;
        }
    }
    
    
    
}