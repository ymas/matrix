﻿using System;
using System.Collections.Generic;

namespace Matrix.Models.ModelViews
{
    public class HomeModelView
    {
        public List<TopEmployeesWidget> TopEmployeesWidget { get; set; }
        public GeneralStatisticsWidgets TotalUsersWidgets { get; set; }
        public GeneralStatisticsWidgets TotalKpisWidgets { get; set; }
        public GeneralStatisticsWidgets TotalEnablersWidgets { get; set; }
        public GeneralStatisticsWidgets TotalFilesWidgets { get; set; }
        public GeneralStatisticsWidgets TotalTasksWidgets { get; set; }
    }
    
    public class PillarsWidgets
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string KpisRate { get; set; }
        public string EnablersRate { get; set; }
        public double OverAllRate { get; set; }

        public PillarsWidgets()
        {
            
        }
        public PillarsWidgets(Guid id,string name, double? resultsAvg, double? enablerAvg, double? overAllRate)
        {
            Id = id;
            Name = name;
            KpisRate = (resultsAvg ?? 0) + "%";
            EnablersRate = (enablerAvg ?? 0) + "%";
            OverAllRate = Math.Round(overAllRate ?? 0, 1);
        }
    }
    
    
    public class TopEmployeesWidget
    {
        public string Name { get; set; }
        public string TeamName { get; set; }
        public string Avg { get; set; }

        public TopEmployeesWidget(string _name, string _team, float? _avg)
        {
            Name = _name;
            TeamName = _team;
            Avg = (_avg ?? 0) +"%";
        }
    }
    
    
    public class GetMainStandards
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int TotalKpis { get; set; }
        public int TotalEnablers { get; set; }
        public double TeamProcessRate { get; set; }
        public string CompletionProgress { get; set; }
        public string TasksRate { get; set; }

        public GetMainStandards()
        {
        }

        public GetMainStandards(Guid? _id, string _name, int _totalkpis , int totalEnablers , double teamProcess , float? _completionProgress)
        {
            Id = _id ?? Guid.NewGuid();
            Name = _name;
            TotalKpis = _totalkpis ;
            TotalEnablers = totalEnablers ;
            TeamProcessRate = teamProcess;
            CompletionProgress = _completionProgress+"%" ?? "0%";
        }
    }
    
    
    
    public class GeneralStatisticsWidgets
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public GeneralStatisticsWidgets(string _name, string _value)
        {
            Name = _name;
            Value = _value;
        }
    }
}