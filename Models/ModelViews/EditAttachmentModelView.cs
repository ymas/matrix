using System;
using System.ComponentModel.DataAnnotations;
using Matrix.Resources;
using Microsoft.AspNetCore.Http;

namespace Matrix.Models.ModelViews
{
    public class EditAttachmentModelView
    {
        public Guid Id { get; set; }
        
        [StringLength(1000)]
        [Display(Name = "NameAr", ResourceType = typeof(_Translate))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(_Translate))]
        public string NameAr { get; set; }

        [StringLength(1000)]
        [Display(Name = "NameEn", ResourceType = typeof(_Translate))]
        [Required(ErrorMessageResourceName = "Required", ErrorMessageResourceType = typeof(_Translate))]
        public string NameEn { get; set; }

        [StringLength(100)]
        [Display(Name ="FileType", ResourceType =typeof(_Translate))]
        public string FileMime { get; set; }

        [Display(Name ="FileSize", ResourceType =typeof(_Translate))]
        public double? FileSize { get; set; }

        [Display(Name ="FileData", ResourceType =typeof(_Translate))]
        public byte[] FileData { get; set; }        
        
        [Display(Name ="TheTeam", ResourceType =typeof(_Translate))]
        public Guid TeamId { get; set; }

        [StringLength(4000)]
        [Display(Name = "FileUrl", ResourceType = typeof(_Translate))]
        public string FileUrl { get; set; } 

        [Display(Name ="SetPrivate" , ResourceType =typeof(_Translate))]
        public bool IsPrivate { get; set; }
        
        [Display(Name = "File", ResourceType = typeof(_Translate))]
        public IFormFile UpFile { get; set; }

        // kpi, enabler
        public string OwnerType { get; set; }
        
        //item owner id 
        public Guid OwnerId { get; set; }


    }
}