﻿
using System.Security.Claims;
using Matrix.Resources;
using Microsoft.AspNetCore.Http;

namespace Matrix
{
    public class Layout
    {
        public string Lang { get; set; }
        public string UserFirstName { get; set; }
        public string PageTitle { get; set; }
        private readonly HttpContext _httpContext;
        public Layout(HttpContext httpContext)
        {
            _httpContext = httpContext;
        }
        
        public Layout()
        {
            _httpContext.Request.Query.TryGetValue("culture", out var parmLang);
            var cookieLang = _httpContext.Request.Cookies["culture"];
            var identity = _httpContext.User.Identity;
            var firstName = ((ClaimsIdentity)identity).FindFirst("FirstName")?.ToString();
            
            
            Lang = (parmLang.ToString() ?? cookieLang) ?? "ar";
            UserFirstName = firstName.Replace("FirstName", "").Replace(":", "") ?? "unknown";
            PageTitle = _Translate.App_Title; // by default you can change it in every model view
        }
    }
}

