﻿using System;
using System.Collections.Generic;

namespace Matrix.Models
{
    public class Notifications
    {
        public int TotalChatsMessages { get; set; }
        public int TotalTasks { get; set; }
        public int TotalAlerts { get; set; }
        public List<ChatsNotifications> ChatsNotifications { get; set; }
        public List<AlertsNotifications> AlertsNotifications { get; set; }
        public List<TasksNotifications> TasksNotifications { get; set; }
    }

    public class ChatsNotifications
    {
        public Guid MessageId { get; set; }
        public Guid WriterId { get; set; }
        public string Name { get; set; }
        public string TimeSpan { get; set; }
        public string PostMessage { get; set; }
        public string WriterphotoLink { get; set; }
    }

    public class TasksNotifications
    {
        public Guid TaskId { get; set; }
        public double Percentage { get; set; }
        public string TaskName { get; set; }
        public string StatusColor { get; set; }
    }


    public class AlertsNotifications
    {
        public Guid TaskId { get; set; }
        public double Percentage { get; set; }
        public string TaskName { get; set; }
        public string StatusColor { get; set; }
    }

   

    public class UsersPermission
    {
        public List<Guid> StandardId { get; set; }
        public Guid UserId { get; set; }
    }

    public class CheckList
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Checked { get; set; }
    }



  
}

   