namespace Matrix.Models.ModelViews
{
    public class CheckListModelView
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public bool IsChecked { get; set; }

        public CheckListModelView(string text = null, string value = null, bool isChecked = false)
        {
            Text = text;
            Value = value;
            IsChecked = isChecked;
        }

        public CheckListModelView()
        {
        }
    }
}