﻿using System.Linq;

namespace Cid_Criminal.Models
{
    public class TopLogModelView
    {
        public IOrderedEnumerable<IGrouping<int, PhotoLog>> TopPhotos { get; set; }
        public IOrderedEnumerable<IGrouping<string, SearchLog>> TopSearches { get; set; }
    }
}