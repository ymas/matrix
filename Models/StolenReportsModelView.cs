﻿using System.Collections.Generic;

namespace Cid_Criminal.Models
{
    public class StolenReportsModelView
    {
        public string CatName { get; set; }
        public List<string> Values { get; set; }
    }
}