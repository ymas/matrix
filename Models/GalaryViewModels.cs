﻿using System;
using System.Collections.Generic;
using Matrix.Data.DomainClasses;

namespace Cid_Criminal.Models
{
    public class GalaryViewModels
    {
        public int Id { get; set; }
        public string  CataName { get; set; }
        public string ParentCatName { get; set; }
        public int? ParentID { get; set; }

        public List<GalaryCategory> ChildCats { get; set; }

//        public virtual ICollection<Galary_Photos> Photos { get; set; }
    }
}