﻿var gulp = require('gulp'),
rename = require('gulp-rename'),
uglify = require('gulp-uglify'),
concat = require('gulp-concat'),
uglifycss = require('gulp-uglifycss'),

rubySass = require('gulp-ruby-sass'),
del = require("del");
var config = require('./npm-config.json');

gulp.task('ar', () =>
    rubySass('src/sass/ar.scss')
        .on('error', rubySass.logError)
        .pipe(uglifycss({
            "uglyComments": false
        }))
        .pipe(gulp.dest('wwwroot/css/'))
);

gulp.task('en', () =>
    rubySass('src/sass/en.scss')
        .on('error', rubySass.logError)
        .pipe(uglifycss({
            "uglyComments": false
        }))
        .pipe(gulp.dest('wwwroot/css/'))
);

// gulp.task('en', function () {
//     rubySass('sass/en.scss', {
//       //  style: 'compressed'
//     })
//         .on('error', function (err) {
//             console.error('error', err.message);
//         })
//         .on('error', function (err) {
//             console.error('error', err.message);
//         })
//         .pipe(gulp.dest('wwwroot/css/'));
// });


// gulp.task("js" , function () {
//    // copy from 
//    gulp.src(config.vendor_files.js)
//        .pipe(rename({dirname:''}))
//        .pipe(gulp.dest(config.js));
//    gulp.src(config.js)
//        .pipe(concat('scripts.js'))
//        .pipe(gulp.dest('../'+config.js))
// });

// gulp.task("images", function () {
//     gulp.src(config.vendor_files.images)
//         .pipe(rename({dirname:''}))
//         .pipe(gulp.dest(config.images))
// });
//
// gulp.task("css", function () {
//     gulp.src(config.vendor_files.css)
//         .pipe(rename({dirname:''}))
//         .pipe(gulp.dest(config.css))
// });

// gulp.task("fonts", function () {
//     gulp.src(config.vendor_files.fonts)
//         .pipe(rename({dirname:''}))
//         .pipe(gulp.dest(config.fonts))
// });

// gulp.task("copyJS", function () {
//     gulp.src([
//         "sass/lib/js/jquery.min.js",
//         "sass/lib/js/jquery-validation/dist/jquery.validate.min.js",
//         "sass/lib/js/bootstrap-rtl.min.js",
//         "sass/assets/js/detect.js",
//         "sass/assets/js/fastclick.js",
//         "sass/assets/js/jquery.slimscroll.js",
//         "sass/assets/js/jquery.blockUI.js",
//         "sass/assets/js/waves.js",
//         "sass/assets/js/wow.min.js",
//         "sass/assets/js/jquery.nicescroll.js",
//         "sass/assets/js/jquery.scrollTo.min.js",
//         "Content/plugins/waypoints/lib/jquery.waypoints.js",
//         "Content/plugins/select2/select2.min.js",
//         "Content/plugins/moment/moment.js",
//         "Content/plugins/timepicker/bootstrap-timepicker.min.js",
//         "Content/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js",
//         "Content/plugins/bootstrap-daterangepicker/daterangepicker.js",
//         "Content/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js",
//         "Content/plugins/custombox/dist/custombox.min.js",
//         "Content/plugins/custombox/dist/legacy.min.js",
//         "Content/plugins/bootstrap-sweetalert/sweet-alert.min.js",
//         "Content/plugins/datatables/js/jquery.dataTables.min.js",
//         "Content/plugins/datatables/js/dataTables.bootstrap.min.js",
//         "Content/plugins/datatables/extensions/AutoFill/js/dataTables.autoFill.min.js",
//         "Content/plugins/datatables/extensions/AutoFill/js/autoFill.bootstrap.min.js",
//         "Content/plugins/datatables/extensions/Vuttons/js/dataTables.buttons.min.js",
//         "Content/plugins/datatables/extensions/AutoFill/js/buttons.bootstrap.min.js",
//         "Content/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js",
//         "Content/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js",
//         "node_modules/switchery/standalone/switchery.js",
//         "Content/plugins/bootstrap-sweetalert/sweet-alert.min.js",
//         "sass/assets/js/jquery.cookie.js",
//         "sass/assets/js/jquery.core.js",
//         "sass/assets/js/jquery.app.js",
//         "sass/assets/js/_my.js"
//     ])
//        .pipe(concat('scripts.js'))
//        .pipe(uglify().on('error', function(e){
//            console.log(e);
//        }))
//        .pipe(gulp.dest('Scripts'))
// });



gulp.task("clean", function () {
    return del([config.css]);
});

 
// watch changes
gulp.task('watchcss', function () {
    gulp.watch(['sass/**/*'], ['ar','en']);
});

gulp.task('default', ['ar','en','watchcss']);
//gulp.task('default', ['ar','en', 'watchcss']);